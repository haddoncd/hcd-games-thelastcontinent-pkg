#ifdef hex_hpp
#error Multiple inclusion
#endif
#define hex_hpp

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif

#ifndef Grid_hpp
#error "Please include Grid.hpp before this file"
#endif



namespace hex
{
  f32 RADIUS;
  f32 SQRT_OF_THREE;
  f32 QHEIGHT;

  M2<f32> CC_FROM_HEX;
  M2<f32> HEX_FROM_CC;

  namespace direction
  {
    enum Enum
    {
      E,
      NE,
      NW,
      W,
      SW,
      SE,
      ENUM_COUNT
    };

    V2<f32> faceCentreOffset[ENUM_COUNT];
  }

  namespace corner
  {
    enum Enum
    {
      NE,
      N,
      NW,
      SW,
      S,
      SE,
      ENUM_COUNT
    };

    V2<f32> offset[ENUM_COUNT];
  }

  struct Vec;
  struct Pos;

  Vec canonicalizeDelta(V2<f32> cc);

  namespace grid
  {
    struct Vec
    {
      V2<i16> v;

      bool operator ==(const Vec &that) const;
      bool operator !=(const Vec &that) const;
      Vec  operator -() const;

      operator ::hex::Vec() const;

      V2<f32> cc() const;
    };

    Vec vec(i16 e, i16 ne);

    struct Pos
    {
      V2<i16> p;

      bool operator ==(const Pos &that) const;
      bool operator !=(const Pos &that) const;

      operator ::hex::Pos() const;

      V2<f32> cc() const;
    };

    Pos pos(i16 e, i16 ne);

    Vec  operator +(const Vec &a, const Vec &b);
    void operator +=(Vec &a, const Vec &b);
    Vec  operator -(const Vec &a, const Vec &b);
    void operator -=(Vec &a, const Vec &b);
    Pos  operator +(const Pos &a, const Vec &b);
    Pos  operator +(const Vec &a, const Pos &b);
    void operator +=(Pos &a, const Vec &b);
    Pos  operator -(const Pos &a, const Vec &b);
    Pos  operator -(const Vec &a, const Pos &b);
    void operator -=(Pos &a, const Vec &b);
    Vec  operator -(const Pos &a, const Pos &b);
  }

  struct Vec
  {
    grid::Vec g;
    V2<f32>   d;

    Vec  operator -() const;

    Vec  getCanonicalized();
    void canonicalizeInPlace();

    V2<f32> cc() const;
  };

  Vec vec(V2<f32> cc);

  struct Pos
  {
    grid::Pos g;
    V2<f32>   d;

    Pos  getCanonicalized();
    void canonicalizeInPlace();

    V2<f32> cc() const;
  };

  Pos pos(V2<f32> cc);


  // Vec / V2<f32>
  Vec  operator +(const Vec &a, const V2<f32> &b);
  Vec  operator +(const V2<f32> &a, const Vec &b);
  void operator +=(Vec &a, const V2<f32> &b);
  Vec  operator -(const Vec &a, const V2<f32> &b);
  Vec  operator -(const V2<f32> &a, const Vec &b);
  void operator -=(Vec &a, const V2<f32> &b);

  // Vec / Vec
  Vec  operator +(const Vec &a, const Vec &b);
  void operator +=(Vec &a, const Vec &b);
  Vec  operator -(const Vec &a, const Vec &b);
  void operator -=(Vec &a, const Vec &b);

  // Vec / Pos
  Pos  operator +(const Vec &a, const Pos &b);
  Pos  operator +(const Pos &a, const Vec &b);
  void operator +=(Pos &a, const Vec &b);
  Pos  operator -(const Vec &a, const Pos &b);
  Pos  operator -(const Pos &a, const Vec &b);
  void operator -=(Pos &a, const Vec &b);

  // Pos / V2<f32>
  Pos  operator +(const Pos &a, const V2<f32> &b);
  Pos  operator +(const V2<f32> &a, const Pos &b);
  void operator +=(Pos &a, const V2<f32> &b);
  Pos  operator -(const Pos &a, const V2<f32> &b);
  void operator -=(Pos &a, const V2<f32> &b);

  // Pos / Pos
  Vec  operator -(const Pos &a, const Pos &b);
  Vec  operator -(const Pos &a, const Pos &b);

  namespace offset
  {
    V2<i32> idx(grid::Pos pos);

    grid::Pos pos(i32 x, i32 y);
    grid::Pos pos(V2<i32> idx);

    template <typename T>
    struct pGrid
    {
      Grid<T> *p;

      pGrid(::pGrid<T> g)
      {
        p = g.p;
      }

      V2<i32> size()
      {
        return v2(castOrAssert<i32>(p->size.x), castOrAssert<i32>(p->size.y));
      }

      T &operator[](V2<i32> i)
      {
        assert(inBounds(i));
        return p->grid[(i.x * p->size.y) + i.y];
      }

      T &operator[](grid::Pos pos)
      {
        V2<i32> i = idx(pos);
        return *this[i];
      }

      bool inBounds(V2<i32> i)
      {
        return 0 <= i.x && i.x < p->size.x &&
               0 <= i.y && i.y < p->size.y;
      }

      bool inBounds(grid::Pos pos)
      {
        return inBounds(idx(pos));
      }
    };
  }
}
