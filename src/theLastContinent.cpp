#ifdef theLastContinent_cpp
#error Multiple inclusion
#endif
#define theLastContinent_cpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef PageAllocator_hpp
#error "Please include PageAllocator.hpp before this file"
#endif

#ifndef Str_hpp
#error "Please include Str.hpp before this file"
#endif

#ifndef StrBuf_hpp
#error "Please include StrBuf.hpp before this file"
#endif

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

#ifndef matrix_hpp
#error "Please include matrix.hpp before this file"
#endif

#ifndef List_hpp
#error "Please include List.hpp before this file"
#endif

#ifndef Prng_hpp
#error "Please include Prng.hpp before this file"
#endif

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif

#ifndef assets_cpp
#error "Please include assets.cpp before this file"
#endif

#ifndef ImguiAdaptor_hpp
#error "Please include ImguiAdaptor.hpp before this file"
#endif

#ifndef magicaVoxel_Mesh_hpp
#error "Please include magicaVoxel_Mesh.hpp before this file"
#endif

#ifndef hex_hpp
#error "Please include hex.hpp before this file"
#endif



template <typename S>
bool print(S out, hex::grid::Vec v)
{
  return print(out, "HV"_s, v.v);
}

template <typename S>
bool print(S out, hex::grid::Pos p)
{
  return print(out, "HP"_s, p.p);
}

template <typename S>
bool print(S out, hex::Vec v)
{
  return print(out, v.g, '+', v.d);
}

template <typename S>
bool print(S out, hex::Pos p)
{
  return print(out, p.g, '+', p.d);
}

namespace ImGui
{
  template <typename T>
  void Value(const char* prefix, M4<T> m)
  {
    ImGui::Text(prefix);
    ImGui::Text("%8.3g %8.3g %8.3g %8.3g", m.x0, m.x1, m.x2, m.x3);
    ImGui::Text("%8.3g %8.3g %8.3g %8.3g", m.y0, m.y1, m.y2, m.y3);
    ImGui::Text("%8.3g %8.3g %8.3g %8.3g", m.z0, m.z1, m.z2, m.z3);
    ImGui::Text("%8.3g %8.3g %8.3g %8.3g", m.w0, m.w1, m.w2, m.w3);
  }

  template <typename ...Ts>
  void Print(Ts... args)
  {
    StrBuf::Variable *s;
    sprintLocal(s, args...);
    ImGui::Text(s->zStr());
  }
}



///////////////////////////////////
// Vertex and Instance datatypes //
///////////////////////////////////

struct Vertex2d
{
  V2<f32> pos;
};

struct Vertex4d
{
  V4<f32> pos;
};

struct Vertex3dWithNormal
{
  V3<f32> pos;
  V3<f32> nrm;
};

struct Vertex3dWithVariableSizeAndThickness
{
  V3<f32> basePos;
  V3<f32> thicknessOffset;
};

struct InstanceWithColor
{
  M4<f32> cameraFromModel;
  V3<f32> color;
};

struct InstanceWithNormalsAndColor
{
  M4<f32> cameraFromModel;
  M4<f32> cameraFromModel_normals;
  V3<f32> color;
};

struct InstanceWithNormals
{
  M4<f32> cameraFromModel;
  M4<f32> cameraFromModel_normals;
};

struct InstanceWithSizeThicknessAndColor
{
  M4<f32> cameraFromModel;
  V4<f32> sizeAndThickness;
  V3<f32> color;
};



////////////////
// Mesh types //
////////////////


// Mesh2d

typedef shaderAssets::programs::ldrQuad::InputFormat GpuInput2d;
typedef shaderAssets::programs::hdrQuad::InputFormat GpuInput2d;
typedef shaders::glsl::VertexArrayConfiguration<GpuInput2d, Vertex2d> Mesh2d;

template <>
V2<f32> *GpuInput2d::addressOfModelPos<Vertex2d>(Vertex2d *v)
{
  return &v->pos;
}


// Mesh2dWithInstanceColor

typedef shaderAssets::programs::uiShapes::InputFormat GpuInput2dWithInstanceColor;
typedef shaders::glsl::InstancedVertexArrayConfiguration<GpuInput2dWithInstanceColor,
                                                         Vertex2d, InstanceWithColor>
        Mesh2dWithInstanceColor;

void configureMesh2dWithInstanceColorVertexAttributes(gl3w::Functions *gl)
{
  GpuInput2dWithInstanceColor::configureVertex_modelPosAttribute<Vertex2d>(gl, 0);
}

template <>
void Mesh2dWithInstanceColor::configureVertexAttributes(gl3w::Functions *gl)
{
  configureMesh2dWithInstanceColorVertexAttributes(gl);
}

template <>
V2<f32> *GpuInput2dWithInstanceColor::addressOfVertex_modelPos<Vertex2d>(Vertex2d *v)
{
  return &v->pos;
}

void configureMesh2dWithInstanceColorInstanceAttributes(gl3w::Functions *gl)
{
  GpuInput2dWithInstanceColor::configureInstance_colorAttribute<InstanceWithColor>(gl, 1);
  GpuInput2dWithInstanceColor::configureInstance_cameraFromModelAttribute<InstanceWithColor>(gl, 1);
}

template <>
void Mesh2dWithInstanceColor::configureInstanceAttributes(gl3w::Functions *gl)
{
  configureMesh2dWithInstanceColorInstanceAttributes(gl);
}

template <>
V3<f32> *GpuInput2dWithInstanceColor::addressOfInstance_color<InstanceWithColor>(InstanceWithColor *i)
{
  return &i->color;
}

template <>
M4<f32> *GpuInput2dWithInstanceColor::addressOfInstance_cameraFromModel<InstanceWithColor>(InstanceWithColor *i)
{
  return &i->cameraFromModel;
}


// Mesh4d

typedef shaderAssets::programs::infinite::InputFormat GpuInput4d;
typedef shaders::glsl::VertexArrayConfiguration<GpuInput4d, Vertex4d> Mesh4d;

template <>
V4<f32> *GpuInput4d::addressOfModelPos<Vertex4d>(Vertex4d *v)
{
  return &v->pos;
}


// Mesh3dWithInstanceSizeThicknessAndColor

typedef shaderAssets::programs::boundingBoxes::InputFormat GpuInput3dWithInstanceSizeThicknessAndColor;
typedef shaders::glsl::InstancedVertexArrayConfiguration<GpuInput3dWithInstanceSizeThicknessAndColor,
                                                         Vertex3dWithVariableSizeAndThickness,
                                                         InstanceWithSizeThicknessAndColor>
        Mesh3dWithInstanceSizeThicknessAndColor;

template <>
void Mesh3dWithInstanceSizeThicknessAndColor::configureVertexAttributes(gl3w::Functions *gl)
{
  GpuInput3dWithInstanceSizeThicknessAndColor::
    configureVertex_modelBasePosAttribute<Vertex3dWithVariableSizeAndThickness>(gl, 0);
  GpuInput3dWithInstanceSizeThicknessAndColor::
    configureVertex_modelThicknessOffsetAttribute<Vertex3dWithVariableSizeAndThickness>(gl, 0);
}

template <>
V3<f32> *GpuInput3dWithInstanceSizeThicknessAndColor::addressOfVertex_modelBasePos
  <Vertex3dWithVariableSizeAndThickness>
  (Vertex3dWithVariableSizeAndThickness *v)
{
  return &v->basePos;
}

template <>
V3<f32> *GpuInput3dWithInstanceSizeThicknessAndColor::addressOfVertex_modelThicknessOffset
  <Vertex3dWithVariableSizeAndThickness>
  (Vertex3dWithVariableSizeAndThickness *v)
{
  return &v->thicknessOffset;
}

template <>
void Mesh3dWithInstanceSizeThicknessAndColor::configureInstanceAttributes(gl3w::Functions *gl)
{
  GpuInput3dWithInstanceSizeThicknessAndColor::
    configureInstance_cameraFromModelAttribute<InstanceWithSizeThicknessAndColor>(gl, 1);
  GpuInput3dWithInstanceSizeThicknessAndColor::
    configureInstance_sizeAndThicknessAttribute<InstanceWithSizeThicknessAndColor>(gl, 1);
  GpuInput3dWithInstanceSizeThicknessAndColor::
    configureInstance_colorAttribute<InstanceWithSizeThicknessAndColor>(gl, 1);
}

template <>
M4<f32> *GpuInput3dWithInstanceSizeThicknessAndColor::addressOfInstance_cameraFromModel
  <InstanceWithSizeThicknessAndColor>
  (InstanceWithSizeThicknessAndColor *i)
{
  return &i->cameraFromModel;
}

template <>
V4<f32> *GpuInput3dWithInstanceSizeThicknessAndColor::addressOfInstance_sizeAndThickness
  <InstanceWithSizeThicknessAndColor>
  (InstanceWithSizeThicknessAndColor *i)
{
  return &i->sizeAndThickness;
}

template <>
V3<f32> *GpuInput3dWithInstanceSizeThicknessAndColor::addressOfInstance_color
  <InstanceWithSizeThicknessAndColor>
  (InstanceWithSizeThicknessAndColor *i)
{
  return &i->color;
}


// Mesh3dLitWithInstanceColor

typedef shaderAssets::programs::hdrLitInstanceColorModels::InputFormat GpuInput3dLitWithInstanceColor;
typedef shaders::glsl::InstancedVertexArrayConfiguration<GpuInput3dLitWithInstanceColor,
                                                         Vertex3dWithNormal,
                                                         InstanceWithNormalsAndColor>
        Mesh3dLitWithInstanceColor;

template <>
void Mesh3dLitWithInstanceColor::configureVertexAttributes(gl3w::Functions *gl)
{
  GpuInput3dLitWithInstanceColor::configureVertex_modelPosAttribute<Vertex3dWithNormal>(gl, 0);
  GpuInput3dLitWithInstanceColor::configureVertex_modelNormalAttribute<Vertex3dWithNormal>(gl, 0);
}

template <>
V3<f32> *GpuInput3dLitWithInstanceColor::addressOfVertex_modelPos
  <Vertex3dWithNormal>(Vertex3dWithNormal *v)
{
  return &v->pos;
}

template <>
V3<f32> *GpuInput3dLitWithInstanceColor::addressOfVertex_modelNormal
  <Vertex3dWithNormal>(Vertex3dWithNormal *v)
{
  return &v->nrm;
}

template <>
void Mesh3dLitWithInstanceColor::configureInstanceAttributes(gl3w::Functions *gl)
{
  GpuInput3dLitWithInstanceColor::configureInstance_colorAttribute<InstanceWithNormalsAndColor>(gl, 1);
  GpuInput3dLitWithInstanceColor::configureInstance_cameraFromModelAttribute<InstanceWithNormalsAndColor>(gl, 1);
  GpuInput3dLitWithInstanceColor::configureInstance_cameraFromModel_normalsAttribute<InstanceWithNormalsAndColor>(gl, 1);
}

template <>
V3<f32> *GpuInput3dLitWithInstanceColor::addressOfInstance_color
  <InstanceWithNormalsAndColor>(InstanceWithNormalsAndColor *i)
{
  return &i->color;
}

template <>
M4<f32> *GpuInput3dLitWithInstanceColor::addressOfInstance_cameraFromModel
  <InstanceWithNormalsAndColor>(InstanceWithNormalsAndColor *i)
{
  return &i->cameraFromModel;
}

template <>
M4<f32> *GpuInput3dLitWithInstanceColor::addressOfInstance_cameraFromModel_normals
  <InstanceWithNormalsAndColor>(InstanceWithNormalsAndColor *i)
{
  return &i->cameraFromModel_normals;
}



// MagicaVoxelMesh (effectively Mesh3dLitWithVertexColor)

typedef shaderAssets::programs::hdrLitVertexColorModels::InputFormat GpuInput3dLitWithVertexColor;
typedef shaders::glsl::InstancedVertexArrayConfiguration <GpuInput3dLitWithVertexColor, magicaVoxel::Mesh::Vertex, InstanceWithNormals> MagicaVoxelMesh;

void configureMagicaVoxelVertexAttributes(gl3w::Functions *gl)
{
  GpuInput3dLitWithVertexColor::configureVertex_modelPosAttribute<magicaVoxel::Mesh::Vertex>(gl, 0);
  GpuInput3dLitWithVertexColor::configureVertex_modelNormalAttribute<magicaVoxel::Mesh::Vertex>(gl, 0);
  GpuInput3dLitWithVertexColor::configureVertex_colorAttribute<magicaVoxel::Mesh::Vertex>(gl, 0);
}

template <>
void MagicaVoxelMesh::configureVertexAttributes(gl3w::Functions *gl)
{
  configureMagicaVoxelVertexAttributes(gl);
}

template <>
V3<f32> *GpuInput3dLitWithVertexColor::addressOfVertex_modelPos
  <magicaVoxel::Mesh::Vertex>(magicaVoxel::Mesh::Vertex *v)
{
  return &v->pos;
}

template <>
V3<f32> *GpuInput3dLitWithVertexColor::addressOfVertex_modelNormal
  <magicaVoxel::Mesh::Vertex>(magicaVoxel::Mesh::Vertex *v)
{
  return &v->nrm;
}

template <>
V3<f32> *GpuInput3dLitWithVertexColor::addressOfVertex_color
  <magicaVoxel::Mesh::Vertex>(magicaVoxel::Mesh::Vertex *i)
{
  return &i->clr;
}

void configureMagicaVoxelInstanceAttributes(gl3w::Functions *gl)
{
  GpuInput3dLitWithVertexColor::configureInstance_cameraFromModelAttribute<InstanceWithNormals>(gl, 1);
  GpuInput3dLitWithVertexColor::configureInstance_cameraFromModel_normalsAttribute<InstanceWithNormals>(gl, 1);
}

template <>
void MagicaVoxelMesh::configureInstanceAttributes(gl3w::Functions *gl)
{
  configureMagicaVoxelInstanceAttributes(gl);
}

template <>
M4<f32> *GpuInput3dLitWithVertexColor::addressOfInstance_cameraFromModel
  <InstanceWithNormals>(InstanceWithNormals *i)
{
  return &i->cameraFromModel;
}

template <>
M4<f32> *GpuInput3dLitWithVertexColor::addressOfInstance_cameraFromModel_normals
  <InstanceWithNormals>(InstanceWithNormals *i)
{
  return &i->cameraFromModel_normals;
}



namespace glExt
{
  enum Enum
  {
    ARB_clip_control,
    ARB_multi_draw_indirect,
    ENUM_COUNT
  };

  Str name[Enum::ENUM_COUNT]
  {
    "GL_ARB_clip_control"_s,
    "GL_ARB_multi_draw_indirect"_s,
  };
}

constexpr glExt::Enum REQUIRED_GL_EXTENSIONS[]
{
  glExt::ARB_clip_control,
};


namespace msaaMode
{
  enum Enum
  {
    NONE,
    X2,
    X4,
    X8,
    ENUM_COUNT
  };

  GLsizei samples[ENUM_COUNT]
  {
    1,
    2,
    4,
    8
  };

  ZStr name[ENUM_COUNT]
  {
    "None"_z,
    "2x"_z,
    "4x"_z,
    "8x"_z
  };

  const char *label[ENUM_COUNT]
  {
    name[NONE],
    name[X2],
    name[X4],
    name[X8]
  };
}

namespace uiShape
{
  enum Enum
  {
    HEXAGON,
    ENUM_COUNT
  };

  DEFINE_ENUM_PREINCREMENT

  Str NAME[ENUM_COUNT] =
  {
    "Hexagon"_s
  };

  Vertex2d vertex_data[] =
  {
    { hex::corner::offset[hex::corner::NE] },
    { hex::corner::offset[hex::corner::N ] },
    { hex::corner::offset[hex::corner::NW] },
    { hex::corner::offset[hex::corner::SW] },
    { hex::corner::offset[hex::corner::S ] },
    { hex::corner::offset[hex::corner::SE] },
  };

  u32 vertex_counts[ENUM_COUNT] =
  {
    6
  };

  Vertex2d *vertex_ptrs[ENUM_COUNT] =
  {
    &vertex_data[0]
  };

  GLuint index_data[] =
  {
    0, 1, 2,
    0, 2, 3,
    0, 3, 5,
    5, 3, 4
  };

  u32 index_counts[ENUM_COUNT] =
  {
    12
  };

  GLuint *index_ptrs[ENUM_COUNT] =
  {
    &index_data[0]
  };

  void load(gl3w::Functions *gl, Mesh2dWithInstanceColor *mesh, Enum shape)
  {
    mesh->init(gl, vertex_ptrs[shape], vertex_counts[shape],
                    index_ptrs[shape],  index_counts[shape]);
  }
}

namespace model
{
  enum Enum
  {
    GRASS,
    FLOWER_1,
    FLOWER_2,
    FLOWER_3,
    PINE_SMALL,
    PINE_LARGE,
    CURSOR,
    ENUM_COUNT
  };

  Str NAME[ENUM_COUNT] =
  {
    "Grass"_s,
    "Flower1"_s,
    "Flower2"_s,
    "Flower3"_s,
    "PineSmall"_s,
    "PineLarge"_s,
    "Cursor"_s,
  };

  #pragma warning( push )
  #pragma warning( disable : 4715)
  template <typename T>
  auto get(T *pack, Enum mesh)
  {
    switch (mesh)
    {
      case GRASS:      return pack->grass;
      case FLOWER_1:   return pack->flower_1;
      case FLOWER_2:   return pack->flower_2;
      case FLOWER_3:   return pack->flower_3;
      case PINE_SMALL: return pack->pine_small;
      case PINE_LARGE: return pack->pine_large;
      case CURSOR:     return pack->cursor;
    }
    assert(false);
  }
  #pragma warning( pop )

  DEFINE_ENUM_PREINCREMENT
}



namespace loadedMeshType
{
  enum Enum
  {
    NONE = 0,
    INDIVIDUAL_BUFFERS,
    COMBINED_BUFFER,
    ENUM_COUNT
  };
}

template <typename GpuInputFormat, typename InputVertex, typename InputInstance, u32 MESH_COUNT>
struct MeshSet
{
  loadedMeshType::Enum loadedMeshType;

  // NB. Methods configureVertexAttributes and configureInstanceAttributes of
  // this type must be defined to use the MeshSet.
  typedef shaders::glsl::MultiMeshVertexArrayConfiguration
            <GpuInputFormat,
             InputVertex,
             InputInstance,
             MESH_COUNT>
          MultiMesh;

  union
  {
    shaders::glsl::InstancedVertexArrayConfiguration
      <GpuInputFormat,
       InputVertex,
       InputInstance>
      individualBuffers[MESH_COUNT];
    shaders::glsl::MultiMeshVertexArrayConfiguration
      <GpuInputFormat,
       InputVertex,
       InputInstance,
       MESH_COUNT>
      combinedBuffer;
  };

  void destroy(gl3w::Functions *gl)
  {
    switch (loadedMeshType)
    {
      case loadedMeshType::INDIVIDUAL_BUFFERS:
      {
        for (u32 i = 0; i < MESH_COUNT; ++i)
        {
          if (individualBuffers[i].vao)
          {
            individualBuffers[i].destroy(gl);
          }
        }
      } break;

      case loadedMeshType::COMBINED_BUFFER:
      {
        combinedBuffer.destroy(gl);
      } break;
    }

    loadedMeshType = loadedMeshType::NONE;
  }
};



typedef MeshSet<GpuInput2dWithInstanceColor,
                Vertex2d, InstanceWithColor,
                uiShape::ENUM_COUNT>
        UiShapeMeshSet;

void UiShapeMeshSet::MultiMesh::configureVertexAttributes(gl3w::Functions *gl)
{
  configureMesh2dWithInstanceColorVertexAttributes(gl);
}

void UiShapeMeshSet::MultiMesh::configureInstanceAttributes(gl3w::Functions *gl)
{
  configureMesh2dWithInstanceColorInstanceAttributes(gl);
}



typedef MeshSet<GpuInput3dLitWithVertexColor,
                magicaVoxel::Mesh::Vertex,
                InstanceWithNormals,
                model::ENUM_COUNT>
        MagicaVoxelMeshSet;

void MagicaVoxelMeshSet::MultiMesh::configureVertexAttributes(gl3w::Functions *gl)
{
  configureMagicaVoxelVertexAttributes(gl);
}

void MagicaVoxelMeshSet::MultiMesh::configureInstanceAttributes(gl3w::Functions *gl)
{
  configureMagicaVoxelInstanceAttributes(gl);
}



struct
{
  struct
  {
    bool multiDrawIndirect;
  } glCaps;

  PageAllocator::State frameTmpAllocatorState;
  UiShapeMeshSet uiShapes;
  MagicaVoxelMeshSet meshes;
  V3<f32> meshSizes[model::ENUM_COUNT];
  Mesh4d infinitePlane;
  Mesh3dWithInstanceSizeThicknessAndColor boundingBox;
  Mesh3dLitWithInstanceColor hexColumn;
  Mesh2d screenQuad;
  GLuint quadSampler;
} libState;

struct FrameBuf
{
  V2<u32> size_pixels;
  GLuint  colorTex;
  GLuint  depthBuf;
  GLuint  renderFbo;
  GLenum  renderFboStatus;

  msaaMode::Enum msaaMode;
  GLuint         msaaBuf;
  union
  {
    GLuint msaaFbos[2];
    struct
    {
      GLuint msaaSrcFbo;
      GLuint msaaDestFbo;
    };
  };

  GLenum msaaSrcFboStatus;
  GLenum msaaDestFboStatus;

  void prepare3d(gl3w::Functions *gl)
  {
    gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, renderFbo);
    gl->Viewport(0, 0, size_pixels.x, size_pixels.y);
    gl->PolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    gl->Enable(GL_CULL_FACE);
    gl->Enable(GL_DEPTH_TEST);
    gl->Disable(GL_BLEND);
  }

  void prepare2d(gl3w::Functions *gl, V2<u32> window_size_pixels)
  {
    gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    gl->Viewport(0, 0, window_size_pixels.x, window_size_pixels.y);
    gl->Disable(GL_CULL_FACE);
    gl->Disable(GL_DEPTH_TEST);
    gl->Enable(GL_BLEND);
  }

  void resolveMsaa(gl3w::Functions *gl)
  {
    if (msaaMode == msaaMode::NONE) return;

    gl->BindFramebuffer(GL_READ_FRAMEBUFFER, msaaSrcFbo);
    gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, msaaDestFbo);
    gl->BlitFramebuffer(0, 0, size_pixels.x, size_pixels.y,
                        0, 0, size_pixels.x, size_pixels.y,
                        GL_COLOR_BUFFER_BIT, GL_NEAREST);
    gl->BindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  }
};

f32 HEX_GRID_SCALE = 50.0f;

namespace tileType
{
  enum Enum
  {
    EMPTY,
    NORMAL
  };
}

struct Tile
{
  tileType::Enum type;
  V3<f32> color;
  f32 height;
};

namespace entityType
{
  enum Enum
  {
    GRASS,
    FLOWER_1,
    FLOWER_2,
    FLOWER_3,
    PINE_SMALL,
    PINE_LARGE,
    ENUM_COUNT
  };

  model::Enum model[ENUM_COUNT] =
  {
    model::GRASS,
    model::FLOWER_1,
    model::FLOWER_2,
    model::FLOWER_3,
    model::PINE_SMALL,
    model::PINE_LARGE,
  };
}

struct Entity
{
  hex::Pos         pos;
  entityType::Enum type;
};

struct GameState
{
  PageAllocator              permanentAllocator;

  ImGui::Adaptor            *imguiAdaptorState;

  Prng                       prng;

  hex::Pos                   cameraFocus;

  bool                       grabbedTerrain;
  hex::Pos                   grabbedPos;

  bool                       drawBoundingBoxes;
  bool                       useNearClipPlane;
  bool                       multiDrawEnabled;

  f32                        range;
  f32                        fov;
  f32                        az;
  f32                        el;

  f32                        exposure_log10;

  shaderAssets::structs::Lighting lightingUniforms;

  V3<f32>                    lightDirection;
  f32                        surfaceBlinnPhongGloss;

  bool                       resetVideo;
  bool                       setRenderResByArea;
  f32                        renderScale;
  i32                        renderAreaSqrt;
  msaaMode::Enum             msaaMode;

  f32                        tileAvgHeight;
  hex::offset::pGrid<Tile>   tiles;
  List<Entity>               entities;

  V2<u32>                    prevWindowSize_pixels;
  FrameBuf                   frameBuf;
};

void loadHexColumn(gl3w::Functions *gl, Mesh3dLitWithInstanceColor *mesh)
{
  using namespace hex;

  f32 column_depth = 0.25f;
  f32 bevel_ratio = 1.0f / 16.0f;
  f32 face_ratio = 1.0f - bevel_ratio;
  f32 inv_sqrt_of_two = 0.70710678118654752440084436210485f;

  assert(RADIUS * bevel_ratio < column_depth);

  V3<f32> up_nrm = v3(0.0f, 0.0f, 1.0f);

  V3<f32>  e_nrm = v3(direction::faceCentreOffset[direction::E],  0.0f);
  V3<f32> ne_nrm = v3(direction::faceCentreOffset[direction::NE], 0.0f);
  V3<f32> nw_nrm = v3(direction::faceCentreOffset[direction::NW], 0.0f);
  V3<f32>  w_nrm = v3(direction::faceCentreOffset[direction::W],  0.0f);
  V3<f32> sw_nrm = v3(direction::faceCentreOffset[direction::SW], 0.0f);
  V3<f32> se_nrm = v3(direction::faceCentreOffset[direction::SE], 0.0f);

  V3<f32>  e_b_nrm = inv_sqrt_of_two *  (e_nrm + up_nrm);
  V3<f32> ne_b_nrm = inv_sqrt_of_two * (ne_nrm + up_nrm);
  V3<f32> nw_b_nrm = inv_sqrt_of_two * (nw_nrm + up_nrm);
  V3<f32>  w_b_nrm = inv_sqrt_of_two *  (w_nrm + up_nrm);
  V3<f32> sw_b_nrm = inv_sqrt_of_two * (sw_nrm + up_nrm);
  V3<f32> se_b_nrm = inv_sqrt_of_two * (se_nrm + up_nrm);

  Vertex3dWithNormal vertex_data[] =
  {
    // central face

    { v3(face_ratio * corner::offset[corner::NE], 0.0f), up_nrm },            //  0
    { v3(face_ratio * corner::offset[corner::N],  0.0f), up_nrm },            //  1
    { v3(face_ratio * corner::offset[corner::NW], 0.0f), up_nrm },            //  2
    { v3(face_ratio * corner::offset[corner::SW], 0.0f), up_nrm },            //  3
    { v3(face_ratio * corner::offset[corner::S],  0.0f), up_nrm },            //  4
    { v3(face_ratio * corner::offset[corner::SE], 0.0f), up_nrm },            //  5


    // bevelled edges

    // east
    { v3(face_ratio * corner::offset[corner::SE],         0.0f),  e_b_nrm },  //  6
    { v3(             corner::offset[corner::SE], -bevel_ratio),  e_b_nrm },  //  7
    { v3(             corner::offset[corner::NE], -bevel_ratio),  e_b_nrm },  //  8
    { v3(face_ratio * corner::offset[corner::NE],         0.0f),  e_b_nrm },  //  9

    // north east
    { v3(face_ratio * corner::offset[corner::NE],         0.0f), ne_b_nrm },  // 10
    { v3(             corner::offset[corner::NE], -bevel_ratio), ne_b_nrm },  // 11
    { v3(             corner::offset[corner::N],  -bevel_ratio), ne_b_nrm },  // 12
    { v3(face_ratio * corner::offset[corner::N],          0.0f), ne_b_nrm },  // 13

    // north west
    { v3(face_ratio * corner::offset[corner::N],          0.0f), nw_b_nrm },  // 14
    { v3(             corner::offset[corner::N],  -bevel_ratio), nw_b_nrm },  // 15
    { v3(             corner::offset[corner::NW], -bevel_ratio), nw_b_nrm },  // 16
    { v3(face_ratio * corner::offset[corner::NW],         0.0f), nw_b_nrm },  // 17

    // west
    { v3(face_ratio * corner::offset[corner::NW],         0.0f),  w_b_nrm },  // 18
    { v3(             corner::offset[corner::NW], -bevel_ratio),  w_b_nrm },  // 19
    { v3(             corner::offset[corner::SW], -bevel_ratio),  w_b_nrm },  // 20
    { v3(face_ratio * corner::offset[corner::SW],         0.0f),  w_b_nrm },  // 21

    // south west
    { v3(face_ratio * corner::offset[corner::SW],         0.0f), sw_b_nrm },  // 22
    { v3(             corner::offset[corner::SW], -bevel_ratio), sw_b_nrm },  // 23
    { v3(             corner::offset[corner::S],  -bevel_ratio), sw_b_nrm },  // 24
    { v3(face_ratio * corner::offset[corner::S],          0.0f), sw_b_nrm },  // 25

    // south east
    { v3(face_ratio * corner::offset[corner::S],          0.0f), se_b_nrm },  // 26
    { v3(             corner::offset[corner::S],  -bevel_ratio), se_b_nrm },  // 27
    { v3(             corner::offset[corner::SE], -bevel_ratio), se_b_nrm },  // 28
    { v3(face_ratio * corner::offset[corner::SE],         0.0f), se_b_nrm },  // 29


    // column sides

    // east
    { v3(corner::offset[corner::SE],  -bevel_ratio),  e_nrm },                // 30
    { v3(corner::offset[corner::SE], -column_depth),  e_nrm },                // 31
    { v3(corner::offset[corner::NE], -column_depth),  e_nrm },                // 32
    { v3(corner::offset[corner::NE],  -bevel_ratio),  e_nrm },                // 33

    // north east
    { v3(corner::offset[corner::NE],  -bevel_ratio), ne_nrm },                // 34
    { v3(corner::offset[corner::NE], -column_depth), ne_nrm },                // 35
    { v3(corner::offset[corner::N],  -column_depth), ne_nrm },                // 36
    { v3(corner::offset[corner::N],   -bevel_ratio), ne_nrm },                // 37

    // north west
    { v3(corner::offset[corner::N],   -bevel_ratio), nw_nrm },                // 38
    { v3(corner::offset[corner::N],  -column_depth), nw_nrm },                // 39
    { v3(corner::offset[corner::NW], -column_depth), nw_nrm },                // 40
    { v3(corner::offset[corner::NW],  -bevel_ratio), nw_nrm },                // 41

    // west
    { v3(corner::offset[corner::NW],  -bevel_ratio),  w_nrm },                // 42
    { v3(corner::offset[corner::NW], -column_depth),  w_nrm },                // 43
    { v3(corner::offset[corner::SW], -column_depth),  w_nrm },                // 44
    { v3(corner::offset[corner::SW],  -bevel_ratio),  w_nrm },                // 45

    // south west
    { v3(corner::offset[corner::SW],  -bevel_ratio), sw_nrm },                // 46
    { v3(corner::offset[corner::SW], -column_depth), sw_nrm },                // 47
    { v3(corner::offset[corner::S],  -column_depth), sw_nrm },                // 48
    { v3(corner::offset[corner::S],   -bevel_ratio), sw_nrm },                // 49

    // south east
    { v3(corner::offset[corner::S],   -bevel_ratio), se_nrm },                // 50
    { v3(corner::offset[corner::S],  -column_depth), se_nrm },                // 51
    { v3(corner::offset[corner::SE], -column_depth), se_nrm },                // 52
    { v3(corner::offset[corner::SE],  -bevel_ratio), se_nrm },                // 53
  };

  GLuint index_data[] =
  {
    // central face

    0, 1, 2,
    0, 2, 3,
    0, 3, 5,
    5, 3, 4,


    // bevelled edges

    // east
    6, 7, 8,
    6, 8, 9,

    // north east
    10, 11, 12,
    10, 12, 13,

    // north west
    14, 15, 16,
    14, 16, 17,

    // west
    18, 19, 20,
    18, 20, 21,

    // south west
    22, 23, 24,
    22, 24, 25,

    // south east
    26, 27, 28,
    26, 28, 29,


    // column sides

    // east
    30, 31, 32,
    30, 32, 33,

    // north east
    34, 35, 36,
    34, 36, 37,

    // north west
    38, 39, 40,
    38, 40, 41,

    // west
    42, 43, 44,
    42, 44, 45,

    // south west
    46, 47, 48,
    46, 48, 49,

    // south east
    50, 51, 52,
    50, 52, 53,
  };

  mesh->init(gl, vertex_data, arrayCount(vertex_data), index_data, arrayCount(index_data));
}

void loadScreenQuad(gl3w::Functions *gl, Mesh2d *mesh)
{
  Vertex2d vertex_data[] =
  {
    {{ -1.0f, -1.0f }},
    {{  1.0f, -1.0f }},
    {{ -1.0f,  1.0f }},
    {{  1.0f,  1.0f }},
  };

  GLuint index_data[] =
  {
    0, 1, 2,
    2, 1, 3,
  };

  mesh->init(gl, vertex_data, arrayCount(vertex_data), index_data, arrayCount(index_data));
}

void loadBoundingBox(gl3w::Functions *gl, Mesh3dWithInstanceSizeThicknessAndColor *mesh)
{
  Vertex3dWithVariableSizeAndThickness vertex_data[] =
  {
    {{ 0,  0,  0}, { 0,  0,  0}}, //  0
    {{ 0,  0,  0}, { 0,  1,  1}}, //  1
    {{ 0,  0,  0}, { 1,  0,  1}}, //  2
    {{ 0,  0,  0}, { 1,  1,  0}}, //  3
    {{ 0,  0,  0}, { 1,  1,  1}}, //  4

    {{ 1,  0,  0}, {-0,  0,  0}}, //  5
    {{ 1,  0,  0}, {-0,  1,  1}}, //  6
    {{ 1,  0,  0}, {-1,  0,  1}}, //  7
    {{ 1,  0,  0}, {-1,  1,  0}}, //  8
    {{ 1,  0,  0}, {-1,  1,  1}}, //  9

    {{ 0,  1,  0}, { 0, -0,  0}}, // 10
    {{ 0,  1,  0}, { 0, -1,  1}}, // 11
    {{ 0,  1,  0}, { 1, -0,  1}}, // 12
    {{ 0,  1,  0}, { 1, -1,  0}}, // 13
    {{ 0,  1,  0}, { 1, -1,  1}}, // 14

    {{ 1,  1,  0}, {-0, -0,  0}}, // 15
    {{ 1,  1,  0}, {-0, -1,  1}}, // 16
    {{ 1,  1,  0}, {-1, -0,  1}}, // 17
    {{ 1,  1,  0}, {-1, -1,  0}}, // 18
    {{ 1,  1,  0}, {-1, -1,  1}}, // 19

    {{ 0,  0,  1}, { 0,  0, -0}}, // 20
    {{ 0,  0,  1}, { 0,  1, -1}}, // 21
    {{ 0,  0,  1}, { 1,  0, -1}}, // 22
    {{ 0,  0,  1}, { 1,  1, -0}}, // 23
    {{ 0,  0,  1}, { 1,  1, -1}}, // 24

    {{ 1,  0,  1}, {-0,  0, -0}}, // 25
    {{ 1,  0,  1}, {-0,  1, -1}}, // 26
    {{ 1,  0,  1}, {-1,  0, -1}}, // 27
    {{ 1,  0,  1}, {-1,  1, -0}}, // 28
    {{ 1,  0,  1}, {-1,  1, -1}}, // 29

    {{ 0,  1,  1}, { 0, -0, -0}}, // 30
    {{ 0,  1,  1}, { 0, -1, -1}}, // 31
    {{ 0,  1,  1}, { 1, -0, -1}}, // 32
    {{ 0,  1,  1}, { 1, -1, -0}}, // 33
    {{ 0,  1,  1}, { 1, -1, -1}}, // 34

    {{ 1,  1,  1}, {-0, -0, -0}}, // 35
    {{ 1,  1,  1}, {-0, -1, -1}}, // 36
    {{ 1,  1,  1}, {-1, -0, -1}}, // 37
    {{ 1,  1,  1}, {-1, -1, -0}}, // 38
    {{ 1,  1,  1}, {-1, -1, -1}}, // 39
  };

  GLuint index_data[] =
  {
     0,  8,  5,
     0,  3,  8,
     0, 13,  3,
     0, 10, 13,
    15, 13, 10,
    15, 18, 13,
    15,  8, 18,
    15,  5,  8,
     1, 14, 11,
     1,  4, 14,
     2,  9,  4,
     2,  7,  9,
     9, 16, 19,
     9,  6, 16,
    14, 17, 12,
    14, 19, 17,
    21, 34, 24,
    21, 31, 34,
    22, 29, 27,
    22, 24, 29,
    29, 36, 26,
    29, 39, 36,
    34, 37, 39,
    34, 32, 37,
    20, 33, 30,
    20, 23, 33,
    20, 28, 23,
    20, 25, 28,
    35, 28, 25,
    35, 38, 28,
    35, 33, 38,
    35, 30, 33,
     0, 22, 20,
     0,  2, 22,
     0,  7,  2,
     0,  5,  7,
    25,  7,  5,
    25, 27,  7,
    25, 22, 27,
    25, 20, 22,
     1, 24,  4,
     1, 21, 24,
     3,  9,  8,
     3,  4,  9,
     9, 26,  6,
     9, 29, 26,
    24, 28, 29,
    24, 23, 28,
    11, 34, 31,
    11, 14, 34,
    13, 19, 14,
    13, 18, 19,
    19, 36, 39,
    19, 16, 36,
    34, 38, 33,
    34, 39, 38,
    10, 17, 15,
    10, 12, 17,
    10, 32, 12,
    10, 30, 32,
    35, 32, 30,
    35, 37, 32,
    35, 17, 37,
    35, 15, 17,
     0, 11, 10,
     0,  1, 11,
     0, 21,  1,
     0, 20, 21,
    30, 21, 20,
    30, 31, 21,
    30, 11, 31,
    30, 10, 11,
     2, 24, 22,
     2,  4, 24,
     3, 14,  4,
     3, 13, 14,
    14, 32, 34,
    14, 12, 32,
    24, 33, 23,
    24, 34, 33,
     7, 29,  9,
     7, 27, 29,
     8, 19, 18,
     8,  9, 19,
    19, 37, 17,
    19, 39, 37,
    29, 38, 39,
    29, 28, 38,
     5, 26, 25,
     5,  6, 26,
     5, 16,  6,
     5, 15, 16,
    35, 16, 15,
    35, 36, 16,
    35, 26, 36,
    35, 25, 26,
  };

  mesh->init(gl, vertex_data, arrayCount(vertex_data), index_data, arrayCount(index_data));
}

void loadInfinitePlane(gl3w::Functions *gl, Mesh4d *mesh)
{
  Vertex4d vertex_data[] =
  {
    {{  0,  0,  0,  1 }},
    {{  1,  0,  0,  0 }},
    {{  0,  1,  0,  0 }},
    {{ -1,  0,  0,  0 }},
    {{  0, -1,  0,  0 }},
  };

  GLuint index_data[] =
  {
    0, 1, 2,
    0, 2, 3,
    0, 3, 4,
    0, 4, 1,
  };

  mesh->init(gl, vertex_data, arrayCount(vertex_data), index_data, arrayCount(index_data));
}

M4<f32> translation(V3<f32> v)
{
  return m4<f32>(1, 0, 0, v.x,
                 0, 1, 0, v.y,
                 0, 0, 1, v.z,
                 0, 0, 0,   1);
}

M4<f32> scale(V3<f32> v)
{
  return m4<f32>( v.x,    0,    0, 0,
                    0,  v.y,    0, 0,
                    0,    0,  v.z, 0,
                    0,    0,    0, 1);
}

Str framebufferStatusName(GLuint status)
{
  switch (status)
  {
    case GL_FRAMEBUFFER_COMPLETE:                      return "Complete"_s;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:         return "Incomplete Attachment"_s;
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: return "Incomplete Missing Attachment"_s;
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:        return "Incomplete Draw Buffer"_s;
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:        return "Incomplete Read Buffer"_s;
    case GL_FRAMEBUFFER_UNSUPPORTED:                   return "Unsupported"_s;
    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:        return "Incomplete Multisample"_s;
    default:                                           return "???"_s;
  }
}

extern "C" openglGame_LIB_INIT_FUNCTION(openglGame_LIB_INIT)
{
  bool result = true;
  gl3w::Functions *gl = &resources->gl;

  bool gl_exts[glExt::ENUM_COUNT];
  gl->checkExtensions(glExt::name, gl_exts, glExt::ENUM_COUNT);

  {
    bool missing_exts = false;
    for (u32 i = 0; i < arrayCount(REQUIRED_GL_EXTENSIONS); ++i)
    {
      glExt::Enum ext = REQUIRED_GL_EXTENSIONS[i];
      if (gl_exts[ext]) continue;
      missing_exts = true;
      println(stdout, "Required OpenGL extension not supported: "_s, glExt::name[ext]);
    }

    if (missing_exts)
    {
      println(stdout, "Some required OpenGL extensions are missing, please update your drivers!"_s);
      result = false;
    }
  }

  libState.glCaps.multiDrawIndirect = gl_exts[glExt::ARB_multi_draw_indirect];
  if (libState.glCaps.multiDrawIndirect) println(stdout, "OpenGL driver is multi-draw capable"_s);

  result &= shaderAssets::init(&resources->gl);

  libState.frameTmpAllocatorState.init();

  libState.uiShapes.loadedMeshType = loadedMeshType::NONE;
  libState.meshes.loadedMeshType = loadedMeshType::NONE;
  memset(libState.meshSizes, 0, sizeof(libState.meshSizes));

  loadInfinitePlane (gl, &libState.infinitePlane);
  loadBoundingBox   (gl, &libState.boundingBox);
  loadScreenQuad    (gl, &libState.screenQuad);
  loadHexColumn     (gl, &libState.hexColumn);

  gl->GenSamplers(1, &libState.quadSampler);
  gl->SamplerParameteri(libState.quadSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl->SamplerParameteri(libState.quadSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  return result;
}

extern "C" openglGame_LIB_SHUTDOWN_FUNCTION(openglGame_LIB_SHUTDOWN)
{
  gl3w::Functions *gl = &resources->gl;

  println(stderr, "Shutting down game library..."_s);

  shaderAssets::shutdown();

  {
    u32 pages_in_use = libState.frameTmpAllocatorState.pageNum + 1;
    u32 total_pages = pages_in_use + libState.frameTmpAllocatorState.freePageCount;
    print(stderr,
      "  frame temp allocator:\n"
      "    "_s, fmt(total_pages), " page(s) allocated\n"
      "    "_s, fmt(pages_in_use), " page(s) in use last frame\n"_s
    );
  }

  libState.frameTmpAllocatorState.destroy();

  libState.uiShapes     .destroy(gl);
  libState.meshes       .destroy(gl);
  libState.infinitePlane.destroy(gl);
  libState.boundingBox  .destroy(gl);
  libState.screenQuad   .destroy(gl);
  libState.hexColumn    .destroy(gl);

  gl->DeleteSamplers(1, &libState.quadSampler);
}

extern "C" openglGame_GAME_INIT_FUNCTION(openglGame_GAME_INIT)
{
  bool result = false;
  gl3w::Functions *gl = &resources->gl;

  GameState *state = alloc<GameState>(resources->platformHeap);
  if (resources->state)
  {
    GameState *prev_state = (GameState *)resources->state;

    state->permanentAllocator = prev_state->permanentAllocator;
    state->permanentAllocator.clear();

    resources->platformHeap->free(prev_state);
  }
  else
  {
    state->permanentAllocator.init(resources->platformHeap);
  }
  resources->state = state;

  BREAKABLE_START
  {
    state->imguiAdaptorState = ImGui::Adaptor::allocAndInit(resources->platformHeap,
                                                            &resources->debugResources,
                                                            gl);

    while (args.valid())
    {
      //else
      {
        print(stdout,
        "Warning: Ignoring unexpected option: \""_s,
          args.getArg(), "\"\n"_s
        );
      }

      args.step();
    }

    // from random.org
    state->prng = {0x37ef2075, 0x0efdde18, 0x378757e6, 0x05aa1e80};

    state->grabbedTerrain    = false;
    state->grabbedPos  = {};

    state->drawBoundingBoxes = false;
    state->useNearClipPlane = true;
    state->multiDrawEnabled = false;

    state->range    = 400.0f;
    state->fov      = PI_32 / 2.0f;
    state->az       = 0.0f;
    state->el       = PI_32 * 3.0f / 8.0f;

    state->exposure_log10 = 0.0f;

    state->surfaceBlinnPhongGloss = 4.0f;
    state->lightDirection = v3<f32>(1, 2, 2);

    state->lightingUniforms.surfaceBlinnPhongMultiplier = 0.2f;
    state->lightingUniforms.ambientColor                = 0.4f * v4(0.8f, 0.9f, 1.0f, 0.0f);
    state->lightingUniforms.globalLightColor            = 0.8f * v4(1.0f, 0.8f, 0.6f, 0.0f);

    state->resetVideo          = false;
    state->setRenderResByArea  = false;
    state->renderScale         = 1.0f;
    state->renderAreaSqrt      = 1000;
    state->msaaMode            = msaaMode::X8;

    {
      u32 island_radius = 50;
      f32 min_height = 8.0f;
      f32 max_height = min_height + 2.0f;

      hex::grid::Pos centre = hex::offset::pos(island_radius, island_radius);

      state->cameraFocus = centre;

      state->tileAvgHeight = (min_height + max_height) / 2.0f;
      state->tiles = Grid<Tile>::alloc(&state->permanentAllocator, island_radius * 2, island_radius * 2);
      state->entities.init();

      V2<i32> offset_grid_size = state->tiles.size();

      for (i32 y = 0; y < offset_grid_size.y; ++y)
      {
        for (i32 x = 0; x < offset_grid_size.x; ++x)
        {
          V2<i32> idx = v2(x, y);
          hex::grid::Pos pos = hex::offset::pos(idx);
          hex::grid::Vec from_centre = pos - centre;
          V2<f32> from_centre_cc = from_centre.cc();
          f32 distance_sq_from_centre = dot(from_centre_cc, from_centre_cc);

          if (distance_sq_from_centre < square(island_radius))
          {
            Tile *tile = &state->tiles[idx];
            tile->type = tileType::NORMAL;
            tile->color = v3(state->prng.nextF32(0.0f, 0.05f),
                             state->prng.nextF32(0.3f, 0.35f),
                             state->prng.nextF32(0.05f, 0.1f));
            tile->height = state->prng.nextF32(min_height, max_height);

            auto *link = alloc<List<Entity>::Link>(&state->permanentAllocator);

            link->data.pos = pos;
            link->data.type = (entityType::Enum) (state->prng.next() % entityType::ENUM_COUNT);

            state->entities.append(link);
          }
        }
      }
    }

    state->prevWindowSize_pixels = v2<u32>(0, 0);
    state->frameBuf = {};

    result = true;
  }
  BREAKABLE_END

  return result;
}

template <typename T>
constexpr bool oppositeSigns(T a, T b)
{
  return (a < 0 && 0 < b) ||
         (b < 0 && 0 < a);
}

template <typename T>
struct CountedList
{
  List<T> list;
  u32 count;

  void init()
  {
    list.init();
    count = 0;
  }

  CountedList():
    count(0)
  {}

  T *push(PageAllocator *al)
  {
    ++count;
    auto *link = allocUninit<List<T>::Link>(al);
    list.append(link);
    return &link->data;
  }

  NO_COPY_OR_MOVE(CountedList)
};

struct UiShapeRenderQueueEntry
{
  V3<f32> pos;
  f32     scaleMult;
  V3<f32> color;
};

struct HdrColumnRenderQueueEntry
{
  V3<f32> pos;
  V3<f32> color;
};

struct HdrModelRenderQueueEntry
{
  V3<f32> pos;
  f32     scaleMult;
};

struct BoundingBoxRenderQueueEntry
{
  V3<f32> cornerPos;
  V3<f32> extent;
};

struct RenderQueues
{
  struct
  {
    CountedList<UiShapeRenderQueueEntry>     shapes[uiShape::ENUM_COUNT];
    CountedList<BoundingBoxRenderQueueEntry> boundingBoxes;
  } ui;

  struct
  {
    CountedList<HdrModelRenderQueueEntry>  models[model::ENUM_COUNT];
    CountedList<HdrColumnRenderQueueEntry> columns;
  } hdr;
};

struct Frustrum
{
  V3<f32> pos;

  V3<f32> leftPlaneNormal;
  V3<f32> rightPlaneNormal;
  V3<f32> bottomPlaneNormal;
  V3<f32> topPlaneNormal;

  // Near plane cull is probably not useful - only the tiny pyramid at the
  // top of the frustrum is culled, probably not worth doing the test for.
  // TODO: Confirm this and remove.
  V3<f32> nearPlanePoint;
  V3<f32> nearPlaneNormal;

  bool includeAABB(V3<f32> object_corner, V3<f32> object_extent)
  {
    // To check a point is on the "right side" of a plane, take the dot
    // product of the plane normal with a vector from any point on the plane
    // to the subject point, and check the sign of the result.

    // Since we're not checking a point, add the size to the vector where the
    // extent of the object points toward the plane, artificially inflating
    // the frustrum to account for the size.

    // "Side" checks
    {
      // The position of the camera (the peak of the frustrum pyramid) is on all
      // of the "side" planes. We can use this vector for all of the side checks.
      V3<f32> plane_to_object = object_corner - pos;

      {
        V3<f32> plane_to_far_corner = plane_to_object;
        if (oppositeSigns(object_extent.x, leftPlaneNormal.x)) plane_to_far_corner.x += object_extent.x;
        if (oppositeSigns(object_extent.y, leftPlaneNormal.y)) plane_to_far_corner.y += object_extent.y;
        if (oppositeSigns(object_extent.z, leftPlaneNormal.z)) plane_to_far_corner.z += object_extent.z;

        if (0 < dot(plane_to_far_corner, leftPlaneNormal)) return false;
      }

      {
        V3<f32> plane_to_far_corner = plane_to_object;
        if (oppositeSigns(object_extent.x, rightPlaneNormal.x)) plane_to_far_corner.x += object_extent.x;
        if (oppositeSigns(object_extent.y, rightPlaneNormal.y)) plane_to_far_corner.y += object_extent.y;
        if (oppositeSigns(object_extent.z, rightPlaneNormal.z)) plane_to_far_corner.z += object_extent.z;

        if (0 < dot(plane_to_far_corner, rightPlaneNormal)) return false;
      }

      {
        V3<f32> plane_to_far_corner = plane_to_object;
        if (oppositeSigns(object_extent.x, bottomPlaneNormal.x)) plane_to_far_corner.x += object_extent.x;
        if (oppositeSigns(object_extent.y, bottomPlaneNormal.y)) plane_to_far_corner.y += object_extent.y;
        if (oppositeSigns(object_extent.z, bottomPlaneNormal.z)) plane_to_far_corner.z += object_extent.z;

        if (0 < dot(plane_to_far_corner, bottomPlaneNormal)) return false;
      }

      {
        V3<f32> plane_to_far_corner = plane_to_object;
        if (oppositeSigns(object_extent.x, topPlaneNormal.x)) plane_to_far_corner.x += object_extent.x;
        if (oppositeSigns(object_extent.y, topPlaneNormal.y)) plane_to_far_corner.y += object_extent.y;
        if (oppositeSigns(object_extent.z, topPlaneNormal.z)) plane_to_far_corner.z += object_extent.z;

        if (0 < dot(plane_to_far_corner, topPlaneNormal)) return false;
      }
    }

    // Near plane cull is probably not useful - only the tiny pyramid at the
    // top of the frustrum is culled, probably not worth doing the test for.
    // TODO: Confirm this and remove.
    {
      V3<f32> plane_to_far_corner = object_corner - nearPlanePoint;
      if (oppositeSigns(object_extent.x, nearPlaneNormal.x)) plane_to_far_corner.x += object_extent.x;
      if (oppositeSigns(object_extent.y, nearPlaneNormal.y)) plane_to_far_corner.y += object_extent.y;
      if (oppositeSigns(object_extent.z, nearPlaneNormal.z)) plane_to_far_corner.z += object_extent.z;

      if (0 < dot(plane_to_far_corner, nearPlaneNormal)) return false;
    }

    return true;
  }
};

extern "C" openglGame_GAME_UPDATE_FUNCTION(openglGame_GAME_UPDATE)
{
  GameState *state = (GameState *) resources->state;
  PageAllocator fal = PageAllocator::reuseState(&libState.frameTmpAllocatorState);
  RenderQueues render_qs;
  gl3w::Functions *gl = &resources->gl;
  assets::Assets *assets = (assets::Assets *)resources->assets;
  assets::Statuses *asset_statuses = (assets::Statuses *)resources->assetStatuses;

  {
    bool bail_out = false;

    // Check that all of our assets are available, otherwise don't run the update!
    // TODO: Handle this more gracefully.
    forEachEnum (model, model)
    {
      if (model::get(asset_statuses, model) == openglGame::AssetStatus::UNLOADED)
      {
        bail_out = true;
        println(stderr,
          "Model "_s, model::NAME[model], " is unloaded, skipping update."_s
        );
      }
    }

    if (bail_out) return;
  }

  ImGui::Adaptor::InputState imgui_input_state =
    state->imguiAdaptorState->newFrame(input);

  bool window_exists = 0 < input->windowSize_pixels.x &&
                       0 < input->windowSize_pixels.y;

  if ((state->prevWindowSize_pixels == input->windowSize_pixels) && !state->resetVideo)
  {
    if (window_exists)
    {
      assert(0 < state->frameBuf.size_pixels.x);
      assert(0 < state->frameBuf.size_pixels.y);
      assert(state->frameBuf.renderFboStatus == GL_FRAMEBUFFER_COMPLETE);

      if (state->frameBuf.msaaMode != msaaMode::NONE)
      {
        assert(state->frameBuf.msaaSrcFboStatus == GL_FRAMEBUFFER_COMPLETE);
        assert(state->frameBuf.msaaDestFboStatus == GL_FRAMEBUFFER_COMPLETE);
      }
    }
  }
  else
  {
    // Delete old framebuf
    if (0 < state->frameBuf.size_pixels.x &&
        0 < state->frameBuf.size_pixels.y)
    {
      gl->DeleteFramebuffers( 1, &state->frameBuf.renderFbo);
      gl->DeleteTextures(     1, &state->frameBuf.colorTex);
      gl->DeleteRenderbuffers(1, &state->frameBuf.depthBuf);

      if (state->frameBuf.msaaMode != msaaMode::NONE)
      {
        gl->DeleteRenderbuffers(1, &state->frameBuf.msaaBuf);
        gl->DeleteFramebuffers( 2, state->frameBuf.msaaFbos);
      }

      state->frameBuf = {};
    }

    if (window_exists)
    {
      if (state->setRenderResByArea)
      {
        state->renderScale = state->renderAreaSqrt / sqrt(input->windowSize_pixels.x * input->windowSize_pixels.y);
      }
      else
      {
        state->renderAreaSqrt = state->renderScale * sqrt(input->windowSize_pixels.x * input->windowSize_pixels.y);
      }

      {
        V2<f32> res = state->renderScale * v2<f32>(input->windowSize_pixels);

        state->frameBuf.size_pixels = v2(max((u32)res.x, 1u),
                                         max((u32)res.y, 1u));
      }

      state->frameBuf.msaaMode = state->msaaMode;

      gl->GenTextures(1, &state->frameBuf.colorTex);
      gl->GenRenderbuffers(1, &state->frameBuf.depthBuf);
      gl->GenFramebuffers(1, &state->frameBuf.renderFbo);

      gl->BindTexture(GL_TEXTURE_2D, state->frameBuf.colorTex);
      gl->TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
                     state->frameBuf.size_pixels.x, state->frameBuf.size_pixels.y,
                     0, GL_RGBA, GL_FLOAT, NULL);
      gl->BindTexture(GL_TEXTURE_2D, 0);

      gl->BindRenderbuffer(GL_RENDERBUFFER, state->frameBuf.depthBuf);
      if (state->frameBuf.msaaMode != msaaMode::NONE)
      {
        gl->RenderbufferStorageMultisample
        (
          GL_RENDERBUFFER, msaaMode::samples[state->frameBuf.msaaMode],
          GL_DEPTH_COMPONENT32F, state->frameBuf.size_pixels.x, state->frameBuf.size_pixels.y
        );
      }
      else
      {
        gl->RenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F,
                                state->frameBuf.size_pixels.x, state->frameBuf.size_pixels.y);
      }
      gl->BindRenderbuffer(GL_RENDERBUFFER, 0);

      if (state->frameBuf.msaaMode != msaaMode::NONE)
      {
        gl->GenRenderbuffers(1, &state->frameBuf.msaaBuf);
        gl->GenFramebuffers( 2, state->frameBuf.msaaFbos);

        gl->BindRenderbuffer(GL_RENDERBUFFER, state->frameBuf.msaaBuf);
        gl->RenderbufferStorageMultisample
        (
          GL_RENDERBUFFER, msaaMode::samples[state->frameBuf.msaaMode], GL_RGBA16F,
          state->frameBuf.size_pixels.x, state->frameBuf.size_pixels.y
        );
        gl->BindRenderbuffer(GL_RENDERBUFFER, 0);


        gl->BindFramebuffer(GL_READ_FRAMEBUFFER, state->frameBuf.msaaSrcFbo);
        gl->FramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, state->frameBuf.msaaBuf);
        state->frameBuf.msaaSrcFboStatus = gl->CheckFramebufferStatus(GL_READ_FRAMEBUFFER);

        if (state->frameBuf.msaaSrcFboStatus != GL_FRAMEBUFFER_COMPLETE)
        {
          println(stdout,
            "MSAA Src FBO Status: "_s,
              framebufferStatusName(state->frameBuf.msaaSrcFboStatus),
              fmt(" (%#X)", state->frameBuf.msaaSrcFboStatus)
          );
          assert(false);
        }

        gl->BindFramebuffer(GL_READ_FRAMEBUFFER, 0);


        gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, state->frameBuf.msaaDestFbo);
        gl->FramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, state->frameBuf.colorTex, 0);
        state->frameBuf.msaaDestFboStatus = gl->CheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

        if (state->frameBuf.msaaDestFboStatus != GL_FRAMEBUFFER_COMPLETE)
        {
          println(stdout,
            "MSAA Dest FBO Status: "_s,
              framebufferStatusName(state->frameBuf.msaaDestFboStatus),
              fmt(" (%#X)", state->frameBuf.msaaDestFboStatus)
          );
          assert(false);
        }

        gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      }

      gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, state->frameBuf.renderFbo);
      if (state->frameBuf.msaaMode == msaaMode::NONE)
      {
        gl->FramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, state->frameBuf.colorTex, 0);
      }
      else
      {
        gl->FramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, state->frameBuf.msaaBuf);
      }
      gl->FramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, state->frameBuf.depthBuf);
      state->frameBuf.renderFboStatus = gl->CheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

      if (state->frameBuf.renderFboStatus != GL_FRAMEBUFFER_COMPLETE)
      {
        println(stdout,
          "Render FBO Status: "_s,
            framebufferStatusName(state->frameBuf.msaaDestFboStatus),
            fmt(" (%#X)", state->frameBuf.msaaDestFboStatus)
        );
        assert(false);
      }

      gl->BindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    }

    println(stdout,
      "Resize: "_s, fmt(input->windowSize_pixels.x), " x "_s, fmt(input->windowSize_pixels.y),
        " -> "_s, fmt(state->frameBuf.size_pixels.x), " x "_s, fmt(state->frameBuf.size_pixels.y),
        " (MSAA: "_s, msaaMode::name[state->frameBuf.msaaMode], ")"_s
    );

    state->prevWindowSize_pixels = input->windowSize_pixels;
    state->resetVideo = false;
  }

  ///////////////
  // Debug GUI //
  ///////////////

  bool debug_window_open = ImGui::Begin("The Last Continent", 0, ImVec2(400, 410), 0.5f, 0);

  if (debug_window_open)
  {
    {
      V2<f32> pos = state->cameraFocus.cc() * HEX_GRID_SCALE;
      bool modified = false;
      modified |= ImGui::SliderFloat("Camera X", &pos.x, -500.0f, 500.0f);
      modified |= ImGui::SliderFloat("Camera Y", &pos.y, -500.0f, 500.0f);

      if (modified)
      {
        state->cameraFocus = hex::pos(pos / HEX_GRID_SCALE);
        state->cameraFocus.canonicalizeInPlace();
      }
    }

    ImGui::Checkbox("Draw Bounding Boxes", &state->drawBoundingBoxes);

    ImGui::SliderFloat("Range", &state->range, 0.0f, 800.0f);
    ImGui::SliderFloat("FoV", &state->fov, TAU_32 / 4096.0f, TAU_32 / 2.0f);
    ImGui::SliderFloat("Azimuth", &state->az, -TAU_32 / 2.0f, TAU_32 / 2.0f);
    ImGui::SliderFloat("Elevation", &state->el, -TAU_32 / 4.0f, TAU_32 / 4.0f);

    ImGui::SliderFloat("Exposure (log10)", &state->exposure_log10, -5.0f, 1.0f);

    ImGui::InputFloat3("Light Direction", state->lightDirection.v);

    ImGui::SliderFloat("Spec Gloss", &state->surfaceBlinnPhongGloss, 1.0f, 16.0f);
    state->lightingUniforms.surfaceBlinnPhongExponent = pow(2, state->surfaceBlinnPhongGloss);

    ImGui::SliderFloat("Spec Mult", &state->lightingUniforms.surfaceBlinnPhongMultiplier, 0.0f, 2.0f);
    ImGui::ColorEdit3("Ambient", state->lightingUniforms.ambientColor.v);
    ImGui::ColorEdit3("Directional", state->lightingUniforms.globalLightColor.v);

    if (libState.glCaps.multiDrawIndirect)
    {
      ImGui::Checkbox("Use MultiDraw", &state->multiDrawEnabled);
    }
    else
    {
      state->multiDrawEnabled = false;
    }

    if (ImGui::SliderFloat("Res (Scale)", &state->renderScale, 0.0f, 2.0f))
    {
      state->resetVideo = true;
      state->setRenderResByArea = false;
    }

    if (ImGui::SliderInt("Res (Px^2)", &state->renderAreaSqrt, 1, 3072))
    {
      state->resetVideo = true;
      state->setRenderResByArea = true;
    }

    state->resetVideo |= ImGui::Combo("MSAA", (int *)&state->msaaMode, msaaMode::label, (int)msaaMode::ENUM_COUNT);

    ImGui::Print("Camera Focus: "_s, state->cameraFocus);
    if (state->grabbedTerrain) ImGui::Print("Grabbed: "_s, state->grabbedPos);
    else ImGui::Text("(not grabbed)");
  }



  ///////////////////
  // Manage Assets //
  ///////////////////

  if (libState.uiShapes.loadedMeshType != loadedMeshType::NONE &&
      state->multiDrawEnabled ? libState.uiShapes.loadedMeshType != loadedMeshType::COMBINED_BUFFER
                              : libState.uiShapes.loadedMeshType != loadedMeshType::INDIVIDUAL_BUFFERS)
  {
    // We have the wrong type of mesh loaded, so unload everything.
    libState.uiShapes.destroy(gl);
  }

  if (!state->multiDrawEnabled)
  {
    if (libState.uiShapes.loadedMeshType == loadedMeshType::NONE)
    {
      libState.uiShapes.loadedMeshType = loadedMeshType::INDIVIDUAL_BUFFERS;
      // To switch into INDIVIDUAL_BUFFERS mode, we only need to zero the
      // data, since we'll load meshes for anything with a zero VAO below.
      zeroArray(libState.uiShapes.individualBuffers);
    }

    forEachEnum(s, uiShape)
    {
      if (!libState.uiShapes.individualBuffers[s].vao)
      {
        uiShape::load(gl, &libState.uiShapes.individualBuffers[s], s);
      }
    }
  }
  else if (/* state->multiDrawEnabled && */ libState.uiShapes.loadedMeshType == loadedMeshType::NONE)
  {
    // TODO: other shapes
    assert(uiShape::ENUM_COUNT == 1);

    libState.uiShapes.loadedMeshType = loadedMeshType::COMBINED_BUFFER;

    libState.uiShapes.combinedBuffer.init(gl, uiShape::vertex_ptrs, uiShape::vertex_counts,
                                              uiShape::index_ptrs,  uiShape::index_counts);
  }


  if (libState.meshes.loadedMeshType != loadedMeshType::NONE &&
      state->multiDrawEnabled ? libState.meshes.loadedMeshType != loadedMeshType::COMBINED_BUFFER
                              : libState.meshes.loadedMeshType != loadedMeshType::INDIVIDUAL_BUFFERS)
  {
    // We have the wrong type of mesh loaded, so unload everything.
    libState.meshes.destroy(gl);
  }
  else if (libState.meshes.loadedMeshType != loadedMeshType::NONE)
  {
    // We have the right type of mesh loaded, but we still need to check for reloaded assets...
    forEachEnum (model, model)
    {
      if (model::get(asset_statuses, model) == openglGame::AssetStatus::RELOADED)
      {
        if (libState.meshes.loadedMeshType == loadedMeshType::COMBINED_BUFFER)
        {
          // if *any* part of our combined buffer has been reloaded, we need to reload the whole thing.
          libState.meshes.destroy(gl);
          break;
        }
        else if (libState.meshes.loadedMeshType == loadedMeshType::INDIVIDUAL_BUFFERS &&
                 libState.meshes.individualBuffers[model].vao)
        {
          // this individual buffer is out of date, so kill it.
          libState.meshes.individualBuffers[model].destroy(gl);
        }
      }
    }
  }

  if (!state->multiDrawEnabled)
  {
    if (libState.meshes.loadedMeshType == loadedMeshType::NONE)
    {
      libState.meshes.loadedMeshType = loadedMeshType::INDIVIDUAL_BUFFERS;
      zeroArray(libState.meshes.individualBuffers);
    }

    forEachEnum (m, model)
    {
      if ( ( model::get(asset_statuses, m) ==
               openglGame::AssetStatus::RELOADED ) &&
           libState.meshes.individualBuffers[m].vao )
      {
        libState.meshes.individualBuffers[m].destroy(gl);
      }

      if (openglGame::AssetStatus::isLoaded[model::get(asset_statuses, m)] && !libState.meshes.individualBuffers[m].vao)
      {
        magicaVoxel::Mesh *mesh_asset = model::get(assets, m);
        libState.meshes.individualBuffers[m].init(gl, mesh_asset->vertices, mesh_asset->vertexCount,
                                        mesh_asset->indices, mesh_asset->indexCount);
        libState.meshSizes[m] = mesh_asset->size;
      }
    }
  }
  else if (/* state->multiDrawEnabled && */ libState.meshes.loadedMeshType == loadedMeshType::NONE)
  {
    bool all_models_loaded = true;
    forEachEnum (m, model)
    {
      all_models_loaded &= openglGame::AssetStatus::isLoaded[model::get(asset_statuses, m)];
    }

    if (all_models_loaded)
    {
      libState.meshes.loadedMeshType = loadedMeshType::COMBINED_BUFFER;

      magicaVoxel::Mesh::Vertex *verts[model::ENUM_COUNT];
      u32                     v_counts[model::ENUM_COUNT];
      u32                     *indices[model::ENUM_COUNT];
      u32                     i_counts[model::ENUM_COUNT];

      forEachEnum (m, model)
      {
        magicaVoxel::Mesh *mesh_asset = model::get(assets, m);
           verts[m] = mesh_asset->vertices;
        v_counts[m] = mesh_asset->vertexCount;
         indices[m] = mesh_asset->indices;
        i_counts[m] = mesh_asset->indexCount;
      }

      libState.meshes.combinedBuffer.init(gl, verts, v_counts, indices, i_counts);
    }
  }



  ///////////////////////////////////
  // Update camera orientation etc //
  ///////////////////////////////////

  if (!imgui_input_state.stoleMouse)
  {
    f32 scroll_zoom_rate = 0.1f;
    state->range *= 1.0f - (scroll_zoom_rate * input->mouse.scroll_wheelClicks);
  }

  if (!imgui_input_state.stoleKeyboard)
  {
    f32 rotate_direction = 0.0f;

    if (input->keyboard.keys[openglGame::KeyboardKey::Q].isDown)
    {
      rotate_direction += 1.0f;
    }

    if (input->keyboard.keys[openglGame::KeyboardKey::E].isDown)
    {
      rotate_direction -= 1.0f;
    }

    f32 rotate_speed = 1.0f;

    state->az +=  rotate_speed * input->timeStep_seconds * rotate_direction;
  }



  //////////////////////////
  // Calculate transforms //
  //////////////////////////

  M4<f32> camera_from_local;
  M4<f32> local_from_camera;
  M4<f32> clip_from_camera;
  M4<f32> camera_from_clip;

  Frustrum camera_in_local;

  if (window_exists)
  {
    // Init Camera Transform (aka. Camera from Local)
    //
    // Local Space
    //  - undistorted world units
    //  - origin at "sea level", at centre of screen
    //  - the ground is an x-y plane
    //  - +z is up
    //  - Right-handed
    //
    // Camera Space
    //  - undistorted world units
    //  - origin at camera
    //  - +x is right
    //  - +y is up
    //  - +z is forward
    //  - Left-handed
    //
    // 1. Rotate by az, anti-clockwise about the z (vertical) axis.
    // 2. Rotate about the x (left-right) axis, to point +z "forward"
    //    (previously +y) at an angle of el "downward" (previously -z), while
    //    flipping y to keep +y pointing "up"
    // 3. Move "forward" (+z) by range (effectively moving the camera "back").
    //   ┌                ┐   ┌                         ┐   ┌                         ┐
    //   │ 1  0  0      0 │   │ 1        0         0  0 │   │ cos(az)  -sin(az)  0  0 │
    //   │ 0  1  0      0 │ . │ 0  sin(el)   cos(el)  0 │ . │ sin(az)   cos(az)  0  0 │
    //   │ 0  0  1  range │   │ 0  cos(el)  -sin(el)  0 │   │       0         0  1  0 │
    //   │ 0  0  0      1 │   │ 0        0         0  1 │   │       0         0  0  1 │
    //   └                ┘   └                         ┘   └                         ┘
    {
      f32 cos_az = cos(state->az);
      f32 sin_az = sin(state->az);
      f32 cos_el = cos(state->el);
      f32 sin_el = sin(state->el);
      f32 range = state->range;

      camera_from_local = m4<f32>(         cos_az,          -sin_az,                0,                        0,
                                  sin_az * sin_el,  cos_az * sin_el,           cos_el,                        0,
                                  sin_az * cos_el,  cos_az * cos_el,          -sin_el,                    range,
                                                0,                0,                0,                        1);

      local_from_camera = m4<f32>(         cos_az,  sin_az * sin_el,  sin_az * cos_el, -range * sin_az * cos_el,
                                          -sin_az,  cos_az * sin_el,  cos_az * cos_el, -range * cos_az * cos_el,
                                                0,           cos_el,          -sin_el,           range * sin_el,
                                                0,                0,                0,                        1);
    }

    // camera is the intersection of the left, right, bottom and top frustrum planes,
    // and therefore all four include the origin in camera space.
    camera_in_local.pos = (local_from_camera * v4<f32>(0, 0, 0, 1)).xyz;

    // Init Perspective Transform (aka. Clip from Camera)
    //
    // Camera Space (see above)
    //
    // Clip Space
    //  - This is *not* NDC, this is what *becomes* NDC through the
    //    perspective divide; in which every coordinate becomes:
    //      [ x/w, y/w, z/w ]
    //
    // Normalised Device Coordinates (NDC)
    //  - Confusingly, this is the space chosen when calling gl->ClipControl (See GL_ARB_clip_control)
    //    I use gl->ClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE), meaning +y is UP and the Z range is [0,1]
    //  - +x is ALWAYS right.
    //  - z[NDC] is exactly near_plane_z/z[Camera] - since z[Clip] = near_plane_z and w[Clip] = z[Camera]
    //  - z[NDC] = 1 when z[Camera] = near_plane_z (the maximum of the depth buffer's range is at the near plane)
    //  - z[NDC] -> 0 as z[Camera] -> ∞ (the minimum of the depth buffer's range is at infinity)
    //  - Therefore, +z is backward (towards the camera), requiring
    //    gl->ClearDepth(0.0f) & gl->DepthFunc(GL_GEQUAL or GL_GREATER)
    //  - Storing inverse-z backward in the depth buffer should provide better depth precision,
    //    assuming we have gl->ClipControl(..., GL_ZERO_TO_ONE), due to pseudo-logarithmic float precision.
    //    See also: https://developer.nvidia.com/content/depth-precision-visualized
    //  - Near/far plane clipping, however, doesn't work, as z[Clip] = near_plane_z for all points,
    //    (but there is no far plane, and we shouldn't be flying the camera through things anyway)
    //
    {
      f32 aspect_ratio = input->windowSize_points.x / input->windowSize_points.y;
      f32 horizontal_fov_radians = state->fov;
      f32 near_plane_z = 1.0f;

      assert(0.0f < horizontal_fov_radians);
      assert(0.0f < aspect_ratio);
      assert(0.0f < near_plane_z);

      // f32 interplane_dz = far_plane_z - near_plane_z;
      f32 half_plane_width_ratio = tan(horizontal_fov_radians / 2.0f);
      f32 half_plane_height_ratio = half_plane_width_ratio / aspect_ratio;

      clip_from_camera = m4<f32>(1.0f / half_plane_width_ratio, 0, 0, 0,
                                 0, 1.0f / half_plane_height_ratio, 0, 0,
                                 0, 0, 0, near_plane_z,
                                 0, 0, 1.0f, 0);

      camera_from_clip = m4<f32>(half_plane_width_ratio, 0, 0, 0,
                                 0, half_plane_height_ratio, 0, 0,
                                 0, 0, 0, 1.0f,
                                 0, 0, 1.0f / near_plane_z, 0);

      M4<f32> local_from_camera_normals = camera_from_local.transposed();

      camera_in_local.leftPlaneNormal   = (local_from_camera_normals * v4<f32>(-1,  0,  -half_plane_width_ratio, 0)).xyz;
      camera_in_local.rightPlaneNormal  = (local_from_camera_normals * v4<f32>( 1,  0,  -half_plane_width_ratio, 0)).xyz;
      camera_in_local.bottomPlaneNormal = (local_from_camera_normals * v4<f32>( 0, -1, -half_plane_height_ratio, 0)).xyz;
      camera_in_local.topPlaneNormal    = (local_from_camera_normals * v4<f32>( 0,  1, -half_plane_height_ratio, 0)).xyz;
      camera_in_local.nearPlanePoint    = (local_from_camera         * v4<f32>( 0,  0,             near_plane_z, 1)).xyz;
      camera_in_local.nearPlaneNormal   = (local_from_camera_normals * v4<f32>( 0,  0,                       -1, 0)).xyz;
    }
  }



  /////////////////////////
  // Locate Mouse Cursor //
  /////////////////////////

  bool cursor_pos_defined = false;
  hex::Vec cursor_offset;
  hex::Pos cursor_pos;

  if (input->mouse.cursorPresent && window_exists)
  {
    cursor_pos_defined = true;

    f32 cursor_z_plane_height = state->tileAvgHeight;

    V2<f32> cursor_pos_negativeOneToOne = v2(-1.0f, -1.0f) +
      v2<f32>(input->mouse.cursorPos_points.x / input->windowSize_points.x,
              input->mouse.cursorPos_points.y / input->windowSize_points.y) * 2.0f;

    V4<f32> cursor_pos_in_clip = v4(cursor_pos_negativeOneToOne, 0.0f, 1.0f);

    V4<f32> cursor_pos_in_camera = camera_from_clip * cursor_pos_in_clip;

    V4<f32> cursor_ray_from_camera_in_local = local_from_camera * v4(cursor_pos_in_camera.xyz, 0.0f);

    f32 ray_to_z_plane_scale = (cursor_z_plane_height - camera_in_local.pos.z) / cursor_ray_from_camera_in_local.z;

    V2<f32> cursor_pos_on_z_plane_in_local = camera_in_local.pos.xy + (ray_to_z_plane_scale * cursor_ray_from_camera_in_local.xy);

    cursor_offset = hex::vec(cursor_pos_on_z_plane_in_local / HEX_GRID_SCALE);
    cursor_offset.canonicalizeInPlace();
    cursor_pos = state->cameraFocus + cursor_offset;
    cursor_pos.canonicalizeInPlace();


    HdrModelRenderQueueEntry *entry = render_qs.hdr.models[model::CURSOR].push(&fal);

    entry->pos = v3(cursor_pos_on_z_plane_in_local, cursor_z_plane_height);
    entry->scaleMult = 0.5f;
  }



  ////////////////////////////
  // Update camera position //
  ////////////////////////////

  if (input->mouse.cursorPresent && input->mouse.buttons[openglGame::MouseButton::LEFT].isDown && !imgui_input_state.stoleMouse)
  {
    if (!state->grabbedTerrain)
    {
      state->grabbedTerrain = true;
      state->grabbedPos = cursor_pos;
      state->grabbedPos.canonicalizeInPlace();
    }
    else
    {
      cursor_pos = state->grabbedPos;
      state->cameraFocus = state->grabbedPos - cursor_offset;
      state->cameraFocus.canonicalizeInPlace();
    }
  }
  else
  {
    if (input->mouse.cursorPresent) state->grabbedTerrain = false;

    if (!imgui_input_state.stoleKeyboard)
    {
      V2<f32> move_direction = {};

      if (input->keyboard.keys[openglGame::KeyboardKey::D].isDown ||
          input->keyboard.keys[openglGame::KeyboardKey::RIGHT_ARROW].isDown)
      {
        move_direction.x += 1.0f;
      }

      if (input->keyboard.keys[openglGame::KeyboardKey::A].isDown ||
          input->keyboard.keys[openglGame::KeyboardKey::LEFT_ARROW].isDown)
      {
        move_direction.x -= 1.0f;
      }

      if (input->keyboard.keys[openglGame::KeyboardKey::W].isDown ||
          input->keyboard.keys[openglGame::KeyboardKey::UP_ARROW].isDown)
      {
        move_direction.y += 1.0f;
      }

      if (input->keyboard.keys[openglGame::KeyboardKey::S].isDown ||
          input->keyboard.keys[openglGame::KeyboardKey::DOWN_ARROW].isDown)
      {
        move_direction.y -= 1.0f;
      }

      normalize(&move_direction);

      // Rotate movement based on facing direction
      {
        f32 az = state->az;
        move_direction = m2( cos(az), sin(az),
                            -sin(az), cos(az)) * move_direction;
      }

      f32 move_speed = 0.5f * HEX_GRID_SCALE;

      state->cameraFocus +=  move_speed * input->timeStep_seconds * move_direction;
      state->cameraFocus.canonicalizeInPlace();
    }
  }



  ////////////////////
  // Simulate World //
  ////////////////////

  if (cursor_pos_defined)
  {
    f32 height;
    V2<i32> tile_idx = hex::offset::idx(cursor_pos.g);
    if (state->tiles.inBounds(tile_idx))
    {
      Tile *tile = &state->tiles[tile_idx];
      height = tile->height - (1.0f / 4.0f);
    }
    else
    {
      height = 0.1f;
    }

    // Add to render queue

    UiShapeRenderQueueEntry *entry = render_qs.ui.shapes[uiShape::HEXAGON].push(&fal);

    entry->pos = v3((cursor_pos.g - state->cameraFocus).cc() * HEX_GRID_SCALE, height);
    entry->scaleMult = HEX_GRID_SCALE;
    entry->color = v3(0.4f);
  }

  {
    V2<i32> world_size = state->tiles.size();

    // "bottom corner" of tile, from tile centre
    V3<f32> tile_corner_offset = HEX_GRID_SCALE * v3(-1.0f * hex::RADIUS,
                                                     -2.0f * hex::QHEIGHT,
                                                     0.0f);

    V3<f32> tile_extent = HEX_GRID_SCALE * v3(2.0f * hex::RADIUS,
                                              4.0f * hex::QHEIGHT,
                                              0.0f);


    for (i32 x = 0; x < world_size.x; ++x)
    {
      for (i32 y = 0; y < world_size.y; ++y)
      {
        V2<i32> idx = v2(x, y);
        Tile *tile = &state->tiles[idx];

        if (tile->type == tileType::EMPTY) continue;



        //////////////
        // Simulate //
        //////////////

        // nothing here yet...



        ////////////////////////////////
        // Queue column for rendering //
        ////////////////////////////////

        hex::grid::Pos pos = hex::offset::pos(idx);

        V3<f32> column_pos = v3((pos - state->cameraFocus).cc() * HEX_GRID_SCALE, tile->height);

        V3<f32> column_bb_corner = column_pos + tile_corner_offset;

        if (!camera_in_local.includeAABB(column_bb_corner,
                                         tile_extent))
        {
          continue;
        }

        {
          HdrColumnRenderQueueEntry *entry = render_qs.hdr.columns.push(&fal);

          entry->pos = column_pos;
          entry->color = tile->color;
        }

        if (state->drawBoundingBoxes)
        {
          BoundingBoxRenderQueueEntry *entry = render_qs.ui.boundingBoxes.push(&fal);

          entry->cornerPos = column_bb_corner;
          entry->extent = tile_extent;
        }
      }
    }
  }


  forEachLinkData (entity, state->entities)
  {
    f32 tile_height = 0.0f;
    {
      V2<i32> tile_idx = hex::offset::idx(entity->pos.g);
      if (state->tiles.inBounds(tile_idx))
      {
        Tile *tile = &state->tiles[tile_idx];
        tile_height = tile->height - (1.0f / 4.0f);
      }
    }



    //////////////
    // Simulate //
    //////////////

    // Nothing here yet...



    /////////////////////////
    // Queue for rendering //
    /////////////////////////

    model::Enum entity_model = entityType::model[entity->type];

    V3<f32> entity_pos_in_local = v3((entity->pos - state->cameraFocus).cc() * HEX_GRID_SCALE, tile_height);

    f32 entity_scale = 1.0f;

    V3<f32> entity_size = entity_scale * libState.meshSizes[entity_model];
    assert(entity_size != v3(0.0f));

    // Frustrum Cull
    if (!camera_in_local.includeAABB(entity_pos_in_local, entity_size)) continue;

    {
      HdrModelRenderQueueEntry *entry = render_qs.hdr.models[entity_model].push(&fal);

      entry->pos = entity_pos_in_local;
      entry->scaleMult = entity_scale;
    }

    if (state->drawBoundingBoxes)
    {
      BoundingBoxRenderQueueEntry *entry = render_qs.ui.boundingBoxes.push(&fal);

      entry->cornerPos  = entity_pos_in_local;
      entry->extent = entity_size;
    }
  }

  if (debug_window_open)
  {
    forEachEnum (model, model)
    {
      if (render_qs.hdr.models[model].count)
      {
        ImGui::Print(fmt(render_qs.hdr.models[model].count), ' ', model::NAME[model], " hdr models in render queue."_s);
      }
    }

    if (render_qs.hdr.columns.count)
    {
      ImGui::Print(fmt(render_qs.hdr.columns.count), " hdr columns in render queue."_s);
    }

    forEachEnum (shape, uiShape)
    {
      if (render_qs.ui.shapes[shape].count)
      {
        ImGui::Print(fmt(render_qs.ui.shapes[shape].count), ' ', uiShape::NAME[shape], " ui shapes in render queue."_s);
      }
    }

    if (render_qs.ui.boundingBoxes.count)
    {
      ImGui::Print(fmt(render_qs.ui.boundingBoxes.count), " ui bounding boxes in render queue."_s);
    }
  }



  ////////////
  // Render //
  ////////////

  if (window_exists)
  {
    using namespace shaderAssets::structs;

    //////////////////////
    // General Settings //
    //////////////////////

    gl->ClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);
    gl->Enable(GL_FRAMEBUFFER_SRGB);
    gl->FrontFace(GL_CCW);
    gl->CullFace(GL_BACK);
    gl->DepthMask(GL_TRUE);
    gl->DepthFunc(GL_GEQUAL);
    gl->BlendEquation(GL_FUNC_ADD);
    gl->BlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);


    /////////////////
    // LDR 3D Pass //
    /////////////////
    {
      state->frameBuf.prepare3d(gl);

      gl->ClearColor(0, 0, 0, 0);
      gl->ClearDepth(0);
      gl->Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // Render ui shapes from queue
      {
        using namespace shaderAssets::programs::uiShapes;

        if (state->multiDrawEnabled)
        {
          assert(libState.uiShapes.loadedMeshType == loadedMeshType::COMBINED_BUFFER);

          PageAllocator tmp_al = fal;

          u32 total_instance_count = 0;
          GLsizei per_shape_counts[uiShape::ENUM_COUNT];

          forEachEnum (s, uiShape)
          {
            total_instance_count += per_shape_counts[s] = render_qs.ui.shapes[s].count;
          }

          if (total_instance_count)
          {
            auto *instances = allocUninitArray<InstanceWithColor>(&tmp_al, total_instance_count);

            u32 i = 0;

            forEachEnum (s, uiShape)
            {
              u32 prev_i = i;
              CountedList<UiShapeRenderQueueEntry> *q = &render_qs.ui.shapes[s];

              forEachLinkData (entry, q->list)
              {
                M4<f32> local_from_model = translation(entry->pos) * scale(v3(entry->scaleMult));

                instances[i].cameraFromModel = camera_from_local * local_from_model;
                instances[i].color = entry->color;
                ++i;
              }

              assert(i == prev_i + per_shape_counts[s]);
            }

            gl->UseProgram(program);

            clipFromCamera::set(clip_from_camera);

            render<uiShape::ENUM_COUNT>(&libState.uiShapes.combinedBuffer, tmp_al, per_shape_counts, instances);
          }
        }
        else
        {
          assert(libState.uiShapes.loadedMeshType == loadedMeshType::INDIVIDUAL_BUFFERS);

          forEachEnum (s, uiShape)
          {
            CountedList<UiShapeRenderQueueEntry> *q = &render_qs.ui.shapes[s];

            if (q->count)
            {
              PageAllocator tmp_al = fal;

              auto *instances = allocUninitArray<InstanceWithColor>(&tmp_al, q->count);

              {
                u32 i = 0;
                forEachLinkData (entry, q->list)
                {
                  M4<f32> local_from_model = translation(entry->pos) * scale(v3(entry->scaleMult));

                  instances[i].cameraFromModel = camera_from_local * local_from_model;
                  instances[i].color = entry->color;
                  ++i;
                }
                assert(i == q->count);
              }

              gl->UseProgram(program);

              clipFromCamera::set(clip_from_camera);

              render(&libState.uiShapes.individualBuffers[s], q->count, instances);
            }
          }
        }
      }

      // Render bounding boxes from queue
      {
        CountedList<BoundingBoxRenderQueueEntry> *q = &render_qs.ui.boundingBoxes;

        if (q->count)
        {
          using namespace shaderAssets::programs::boundingBoxes;

          PageAllocator tmp_al = fal;

          auto *instances = allocUninitArray<InstanceWithSizeThicknessAndColor>(&tmp_al, q->count);

          {
            u32 i = 0;
            forEachLinkData (entry, q->list)
            {
              M4<f32> local_from_model = translation(entry->cornerPos);

              instances[i].cameraFromModel = camera_from_local * local_from_model;
              instances[i].sizeAndThickness = v4(entry->extent, -0.2f);
              instances[i].color = v3(1.0f, 1.0f, 0.0f);
              ++i;
            }
            assert(i == q->count);
          }

          gl->UseProgram(program);

          clipFromCamera::set(clip_from_camera);

          render(&libState.boundingBox, q->count, instances);
        }
      }

      // Resolve MSAA if enabled
      state->frameBuf.resolveMsaa(gl);

      // Render to screen, resolving resolution scaling.
      {
        using namespace shaderAssets::programs::ldrQuad;

        state->frameBuf.prepare2d(gl, input->windowSize_pixels);

        gl->ClearColor(0, 0, 0, 0);
        gl->Clear(GL_COLOR_BUFFER_BIT);

        gl->UseProgram(program);

        GLint texture_unit = 0;
        gl->ActiveTexture(GL_TEXTURE0 + texture_unit);
        gl->BindTexture(GL_TEXTURE_2D, state->frameBuf.colorTex);
        gl->GenerateMipmap(GL_TEXTURE_2D);
        gl->BindSampler(texture_unit, libState.quadSampler);
        src::set(texture_unit);

        frameBufferSize::set(input->windowSize_pixels);

        render(&libState.screenQuad);

        gl->BindTexture(GL_TEXTURE_2D, 0);
        gl->BindSampler(texture_unit, 0);
      }
    }


    /////////////////
    // HDR 3D Pass //
    /////////////////
    {
      state->frameBuf.prepare3d(gl);

      // Depth buffer already contains LDR result, so don't clear it!
      gl->ClearColor(0, 0, 0, 0);
      gl->Clear(GL_COLOR_BUFFER_BIT);

      state->lightingUniforms.globalLightCameraVector = camera_from_local * v4<f32>(state->lightDirection, 0);
      normalize(&state->lightingUniforms.globalLightCameraVector);

      // Render infinite plane (skybox TBD)
      {
        using namespace shaderAssets::programs::infinite;

        gl->UseProgram(program);
        lighting::set(&state->lightingUniforms);

        clipFromCamera::set(clip_from_camera);
        cameraFromModel::set(camera_from_local);
        cameraFromModel_normals::set(local_from_camera.transposed());

        // Normals should be transformed by the transpose of the inverse
        cameraFromModel_normals::set(local_from_camera.transposed());

        uniformColor::set(v3(0.01f, 0.15f, 0.25f));

        render(&libState.infinitePlane);
      }

      // Render models from queue
      {
        using namespace shaderAssets::programs::hdrLitVertexColorModels;

        if (state->multiDrawEnabled)
        {
          assert(libState.meshes.loadedMeshType == loadedMeshType::COMBINED_BUFFER);

          PageAllocator tmp_al = fal;

          u32 total_instance_count = 0;
          GLsizei per_mesh_counts[model::ENUM_COUNT];

          forEachEnum (m, model)
          {
            total_instance_count += per_mesh_counts[m] = render_qs.hdr.models[m].count;
          }

          if (total_instance_count)
          {
            auto *instances = allocUninitArray<InstanceWithNormals>(&tmp_al, total_instance_count);

            u32 i = 0;

            forEachEnum (m, model)
            {
              u32 prev_i = i;
              CountedList<HdrModelRenderQueueEntry> *q = &render_qs.hdr.models[m];

              forEachLinkData (entry, q->list)
              {
                M4<f32> local_from_model = translation(entry->pos) * scale(v3(entry->scaleMult));
                M4<f32> model_from_local = scale(v3(1.0f/entry->scaleMult)) * translation(-entry->pos);

                instances[i].cameraFromModel = camera_from_local * local_from_model;

                // Normals should be transformed by the transpose of the inverse
                instances[i].cameraFromModel_normals = model_from_local * local_from_camera;
                instances[i].cameraFromModel_normals.transpose();

                ++i;
              }

              assert(i == prev_i + per_mesh_counts[m]);
            }

            gl->UseProgram(program);

            clipFromCamera::set(clip_from_camera);
            lighting::set(&state->lightingUniforms);

            render<model::ENUM_COUNT>(&libState.meshes.combinedBuffer, tmp_al, per_mesh_counts, instances);
          }
        }
        else
        {
          assert(libState.meshes.loadedMeshType == loadedMeshType::INDIVIDUAL_BUFFERS);

          forEachEnum (m, model)
          {
            CountedList<HdrModelRenderQueueEntry> *q = &render_qs.hdr.models[m];

            if (q->count)
            {
              PageAllocator tmp_al = fal;

              auto *instances = allocUninitArray<InstanceWithNormals>(&tmp_al, q->count);

              {
                u32 i = 0;
                forEachLinkData (entry, q->list)
                {
                  M4<f32> local_from_model = translation(entry->pos) * scale(v3(entry->scaleMult));
                  M4<f32> model_from_local = scale(v3(1.0f/entry->scaleMult)) * translation(-entry->pos);

                  instances[i].cameraFromModel = camera_from_local * local_from_model;

                  // Normals should be transformed by the transpose of the inverse
                  instances[i].cameraFromModel_normals = model_from_local * local_from_camera;
                  instances[i].cameraFromModel_normals.transpose();

                  ++i;
                }
                assert(i == q->count);
              }

              gl->UseProgram(program);

              clipFromCamera::set(clip_from_camera);
              lighting::set(&state->lightingUniforms);

              render(&libState.meshes.individualBuffers[m], q->count, instances);
            }
          }
        }
      }

      // Render columns from queue
      {
        CountedList<HdrColumnRenderQueueEntry> *q = &render_qs.hdr.columns;

        if (q->count)
        {
          using namespace shaderAssets::programs::hdrLitInstanceColorModels;

          PageAllocator tmp_al = fal;

          auto *instances = allocUninitArray<InstanceWithNormalsAndColor>(&tmp_al, q->count);

          {
            u32 i = 0;
            forEachLinkData (entry, q->list)
            {
              M4<f32> local_from_model = translation(entry->pos) * scale(v3(HEX_GRID_SCALE));
              M4<f32> model_from_local = scale(v3(1.0f/HEX_GRID_SCALE)) * translation(-entry->pos);

              instances[i].cameraFromModel = camera_from_local * local_from_model;

              // Normals should be transformed by the transpose of the inverse
              instances[i].cameraFromModel_normals = model_from_local * local_from_camera;
              instances[i].cameraFromModel_normals.transpose();

              instances[i].color = entry->color;

              ++i;
            }
            assert(i == q->count);
          }

          gl->UseProgram(program);

          clipFromCamera::set(clip_from_camera);
          lighting::set(&state->lightingUniforms);

          render(&libState.hexColumn, q->count, instances);
        }
      }

      // Resolve MSAA if enabled
      state->frameBuf.resolveMsaa(gl);

      // Render to screen, resolving HDR & resolution scaling
      // Screen framebuffer already contains LDR result, so don't clear!
      {
        using namespace shaderAssets::programs::hdrQuad;

        state->frameBuf.prepare2d(gl, input->windowSize_pixels);

        gl->UseProgram(program);

        GLint texture_unit = 0;
        gl->ActiveTexture(GL_TEXTURE0 + texture_unit);
        gl->BindTexture(GL_TEXTURE_2D, state->frameBuf.colorTex);
        gl->GenerateMipmap(GL_TEXTURE_2D);
        gl->BindSampler(texture_unit, libState.quadSampler);
        src::set(texture_unit);

        frameBufferSize::set(input->windowSize_pixels);
        maxIntensity::set(1.0f / pow(10.0f, state->exposure_log10));

        render(&libState.screenQuad);

        gl->BindTexture(GL_TEXTURE_2D, 0);
        gl->BindSampler(texture_unit, 0);
      }
    }

    // Draw debug UI
    ImGui::End();
    state->imguiAdaptorState->render(gl);
  }
}
