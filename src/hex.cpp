#ifdef hex_cpp
#error Multiple inclusion
#endif
#define hex_cpp

#ifndef hex_hpp
#include "hex.hpp"
#endif



namespace hex
{
  // Coordinate system for a hexagonal grid:

  ///////////////////////////////
  //                           //
  //         y   / \ / \ / \   //
  //            |0,2|1,2|2,2|  //
  //       ^   / \ / \ / \ /   //
  //      /   |0,1|1,1|2,1|    //
  //     /   / \ / \ / \ /     //
  //    /   |0,0|1,0|2,0|      //
  //   /     \ / \ / \ /       //
  //  /                        //
  //  -------------->  x       //
  //                           //
  ///////////////////////////////

  extern f32 RADIUS = 1;
  extern f32 SQRT_OF_THREE = 1.7320508075688772935274463415059;
  extern f32 QHEIGHT = RADIUS / SQRT_OF_THREE;

  extern M2<f32> CC_FROM_HEX = m2<f32>(2 *  RADIUS, 1 *  RADIUS,
                                       0 * QHEIGHT, 3 * QHEIGHT);

  extern M2<f32> HEX_FROM_CC = m2<f32>( 1 / (2 * RADIUS), -1 / (6 * QHEIGHT),
                                             0 * RADIUS,   1 / (3 * QHEIGHT));

  extern V2<f32> direction::faceCentreOffset[] =
  {
    v2<f32>( 1.0 * RADIUS,  0.0 * QHEIGHT), // E
    v2<f32>( 0.5 * RADIUS,  1.5 * QHEIGHT), // NE
    v2<f32>(-0.5 * RADIUS,  1.5 * QHEIGHT), // NW
    v2<f32>(-1.0 * RADIUS,  0.0 * QHEIGHT), // W
    v2<f32>(-0.5 * RADIUS, -1.5 * QHEIGHT), // SW
    v2<f32>( 0.5 * RADIUS, -1.5 * QHEIGHT), // SE
  };

  extern V2<f32> corner::offset[] =
  {
    v2<f32>( 1.0 * RADIUS,  1.0 * QHEIGHT), // NE
    v2<f32>( 0.0 * RADIUS,  2.0 * QHEIGHT), // N
    v2<f32>(-1.0 * RADIUS,  1.0 * QHEIGHT), // NW
    v2<f32>(-1.0 * RADIUS, -1.0 * QHEIGHT), // SW
    v2<f32>( 0.0 * RADIUS, -2.0 * QHEIGHT), // S
    v2<f32>( 1.0 * RADIUS, -1.0 * QHEIGHT), // SE
  };

  Vec canonicalizeDelta(V2<f32> cc)
  {
    V2<f32> hex_f  = HEX_FROM_CC * cc;

    // One odd quirk of skewed bases is that since the basis vectors are not
    // perpendicular to the "lines of constant y" (or x) you can draw a
    // different set of "axes" that are. I'll call these x' and y', and we'll
    // use them in just a second.

    /////////////////////////////////////////////
    //                                         //
    //          / \   / \   / \   / \   / \    //
    //  y'     /   \ /   \ /   \ /   \ /   \   //
    //        |     |     |     |     |     |  //
    //  ^     | 2,-1| 2, 0| 2, 1| 2, 2| 2, 3|  //
    //  |     |     |     |     |     |     |  //
    //  |      \   / \   / \   / \   / \   /   //
    //  |       \ /   \ /   \ /   \ /   \ /    //
    //  |        |     |     |     |     |     //
    //  |        | 1, 0| 1, 1| 1, 2| 1, 3|     //
    //  |        |     |     |     |     |     //
    //  |       / \   / \   / \   / \   / \    //
    //  |      /   \ /   \ /   \ /   \ /   \   //
    //  |     |     |     |     |     |     |  //
    //  |     | 0, 0| 0, 1| 0, 2| 0, 3| 0, 4|  //
    //  |     |     |     |     |     |     |  //
    //   \     \   / \   / \   / \   / \   /   //
    //     \    \ /   \ /   \ /   \ /   \ /    //
    //       \         |     |     |     |     //
    //         \       |-1, 2|-1, 3|-1, 4|     //
    //           \     |     |     |     |     //
    //             \    \   / \   / \   /      //
    //               \   \ /   \ /   \ /       //
    //                 \        |     |        //
    //                   >      |-2, 4|        //
    //                     x'   |     |        //
    //                           \   /         //
    //                            \ /          //
    //                                         //
    /////////////////////////////////////////////

    // "Cube" coordinates address the 2D plane of hexagons as though it were
    // the x + y + z = 0 plane of a 3D cartesian coordinate system. Because of
    // the way this slices through the cube lattice, the integer value-d
    // vertices lie on the hexagonal grid.

    // Since our real basis vectors point out of this plane, we use
    // "flattened" basis vectors:
    // X' = (   1, -0.5, -0.5)
    // Y' = (-0.5,    1, -0.5)
    // Z' = (-0.5, -0.5,    1)

    // Surprise surprise, these vectors are consistent with the x' and y'
    // "axes" we saw earlier:

    //////////////////////
    //                  //
    //        y'        //
    //                  //
    //        ^         //
    //        |         //
    //        |         //
    //       / \        //
    //     /     \      //
    //   <         >    //
    // z'            x' //
    //                  //
    //////////////////////

    // As such, the x and y coordinate of a hex are always equal to the x' and
    // y' coordinate, and by x' + y' + z' = 0, z' = - x - y.


    V3<f32> cube_f = v3(hex_f.x, hex_f.y, - hex_f.x - hex_f.y);
    V3<i16> cube_g = v3(castOrAssert<i16>(round(cube_f.x)),
                        castOrAssert<i16>(round(cube_f.y)),
                        castOrAssert<i16>(round(cube_f.z)));
    V3<f32> cube_d = v3(absDiff(cube_f.x, (f32)cube_g.x),
                        absDiff(cube_f.y, (f32)cube_g.y),
                        absDiff(cube_f.z, (f32)cube_g.z));

    // Rounding like this can take us out of the plane, so we renormalise,
    // correcting whichever coordinate had the largest delta. This gives us
    // the correct round-to-containing-hex behaviour.

    if ((cube_d.y < cube_d.x) && (cube_d.z < cube_d.x))
    {
      cube_g.x = - cube_g.y - cube_g.z;
    }
    else if (cube_d.z < cube_d.y)
    {
      cube_g.y = - cube_g.x - cube_g.z;
    }
    // else z error is greatest,
    // but we won't use it, so don't bother correcting it.

    Vec result;
    result.g.v = cube_g.xy;
    result.d = cc - (CC_FROM_HEX * v2<f32>(result.g.v));
    return result;
  }

  namespace grid
  {
    bool Vec::operator ==(const Vec &that) const
    {
      return this->v == that.v;
    }

    bool Vec::operator !=(const Vec &that) const
    {
      return this->v != that.v;
    }

    Vec Vec::operator -() const
    {
      Vec result;
      result.v = -v;
      return result;
    }

    Vec::operator ::hex::Vec() const
    {
      ::hex::Vec result;
      result.g = *this;
      result.d = v2<f32>(0);
      return result;
    }

    V2<f32> Vec::cc() const
    {
      return CC_FROM_HEX * v2<f32>(v);
    }

    Vec vec(i16 e, i16 ne)
    {
      Vec result;
      result.v.x = e;
      result.v.y = ne;
      return result;
    }

    bool Pos::operator ==(const Pos &that) const
    {
      return this->p == that.p;
    }

    bool Pos::operator !=(const Pos &that) const
    {
      return this->p != that.p;
    }

    Pos::operator ::hex::Pos() const
    {
      ::hex::Pos result;
      result.g = *this;
      result.d = v2<f32>(0);
      return result;
    }

    V2<f32> Pos::cc() const
    {
      return CC_FROM_HEX * v2<f32>(p);
    }

    Pos pos(i16 e, i16 ne)
    {
      Pos result;
      result.p.x = e;
      result.p.y = ne;
      return result;
    }

    Vec operator +(const Vec &a, const Vec &b)
    {
      Vec result;
      result.v = a.v + b.v;
      return result;
    }

    void operator +=(Vec &a, const Vec &b)
    {
      a.v += b.v;
    }

    Vec operator -(const Vec &a, const Vec &b)
    {
      Vec result;
      result.v = a.v - b.v;
      return result;
    }

    void operator -=(Vec &a, const Vec &b)
    {
      a.v -= b.v;
    }

    Pos operator +(const Pos &a, const Vec &b)
    {
      Pos result;
      result.p = a.p + b.v;
      return result;
    }

    Pos operator +(const Vec &a, const Pos &b)
    {
      return b + a;
    }

    void operator +=(Pos &a, const Vec &b)
    {
      a.p += b.v;
    }

    Pos operator -(const Pos &a, const Vec &b)
    {
      Pos result;
      result.p = a.p - b.v;
      return result;
    }

    Pos operator -(const Vec &a, const Pos &b)
    {
      return b - a;
    }

    Vec operator -(const Pos &a, const Pos &b)
    {
      Vec result;
      result.v = a.p - b.p;
      return result;
    }

    void operator -=(Pos &a, const Vec &b)
    {
      a.p -= b.v;
    }
  }

  Vec Vec::operator -() const
  {
    Vec result;
    result.g = -g;
    result.d = -d;
    return result;
  }

  Vec Vec::getCanonicalized()
  {
    return g + canonicalizeDelta(d);
  }

  void Vec::canonicalizeInPlace()
  {
    V2<f32> tmp = d;
    d = v2<f32>(0);
    (*this) += canonicalizeDelta(tmp);
  }

  V2<f32> Vec::cc() const
  {
    return g.cc() + d;
  }

  Vec vec(V2<f32> d)
  {
    Vec result;
    result.g = hex::grid::vec(0, 0);
    result.d = d;
    return result;
  }

  Pos Pos::getCanonicalized()
  {
    return g + canonicalizeDelta(d);
  }

  void Pos::canonicalizeInPlace()
  {
    V2<f32> tmp = d;
    d = v2<f32>(0);
    (*this) += canonicalizeDelta(tmp);
  }

  V2<f32> Pos::cc() const
  {
    return g.cc() + d;
  }

  Pos pos(V2<f32> d)
  {
    Pos result;
    result.g = hex::grid::pos(0, 0);
    result.d = d;
    return result;
  }


  // Vec / V2<f32>

  Vec operator +(const Vec &a, const V2<f32> &b)
  {
    Vec result;
    result.g = a.g;
    result.d = a.d + b;
    return result;
  }

  Vec operator +(const V2<f32> &a, const Vec &b)
  {
    return b + a;
  }

  void operator +=(Vec &a, const V2<f32> &b)
  {
    a.d += b;
  }

  Vec operator -(const Vec &a, const V2<f32> &b)
  {
    return a + (-b);
  }

  Vec operator -(const V2<f32> &a, const Vec &b)
  {
    return a + (-b);
  }

  void operator -=(Vec &a, const V2<f32> &b)
  {
    a.d -= b;
  }


  // Vec / Vec

  Vec operator +(const Vec &a, const Vec &b)
  {
    Vec result;
    result.g = a.g + b.g;
    result.d = a.d + b.d;
    return result;
  }

  void operator +=(Vec &a, const Vec &b)
  {
    a.g += b.g;
    a.d += b.d;
  }

  Vec operator -(const Vec &a, const Vec &b)
  {
    return a + (-b);
  }

  void operator -=(Vec &a, const Vec &b)
  {
    a.g -= b.g;
    a.d -= b.d;
  }


  // Vec / Pos

  Pos operator +(const Vec &a, const Pos &b)
  {
    Pos result;
    result.g = a.g + b.g;
    result.d = a.d + b.d;
    return result;
  }

  Pos operator +(const Pos &a, const Vec &b)
  {
    return b + a;
  }

  void operator +=(Pos &a, const Vec &b)
  {
    a.g += b.g;
    a.d += b.d;
  }

  Pos operator -(const Vec &a, const Pos &b)
  {
    Pos result;
    result.g = a.g - b.g;
    result.d = a.d - b.d;
    return result;
  }

  Pos operator -(const Pos &a, const Vec &b)
  {
    return a + (-b);
  }

  void operator -=(Pos &a, const Vec &b)
  {
    a.g -= b.g;
    a.d -= b.d;
  }


  // Pos / V2<f32>

  Pos operator +(const Pos &a, const V2<f32> &b)
  {
    Pos result;
    result.g = a.g;
    result.d = a.d + b;
    return result;
  }

  Pos operator +(const V2<f32> &a, const Pos &b)
  {
    return b + a;
  }

  void operator +=(Pos &a, const V2<f32> &b)
  {
    a.d += b;
  }

  Pos operator -(const Pos &a, const V2<f32> &b)
  {
    return a + (-b);
  }

  void operator -=(Pos &a, const V2<f32> &b)
  {
    a += -b;
  }


  // Pos / Pos

  Vec operator -(const Pos &a, const Pos &b)
  {
    Vec result;
    result.g = a.g - b.g;
    result.d = a.d - b.d;
    return result;
  }

  namespace offset
  {
    i32 xOffset(i32 y)
    {
      if (0 <= y) return  y / 2;
      else        return ((y+1) / 2) - 1;
    }

    V2<i32> idx(grid::Pos pos)
    {
      return v2<i32>(pos.p) + v2<i32>(xOffset(pos.p.y), 0);
    }

    grid::Pos pos(i32 x, i32 y)
    {
      i32 x_offset = xOffset(y);
      grid::Pos result;
      result.p.x = castOrAssert<i16>(x - x_offset);
      result.p.y = castOrAssert<i16>(y);
      return result;
    }

    grid::Pos pos(V2<i32> idx)
    {
      return pos(idx.x, idx.y);
    }
  }
}
