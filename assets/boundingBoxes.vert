#version 330 core

uniform mat4 clipFromCamera;

layout(location = 0) in vec3 vertex_modelBasePos;
layout(location = 1) in vec3 vertex_modelThicknessOffset;
layout(location = 2) in mat4 instance_cameraFromModel;
layout(location = 6) in vec4 instance_sizeAndThickness;
layout(location = 7) in vec3 instance_color;

smooth out vec3 color;

void main()
{
  vec3 modelPos  = (vertex_modelBasePos * (instance_sizeAndThickness.xyz)) + (vertex_modelThicknessOffset * instance_sizeAndThickness.w);
  gl_Position    = clipFromCamera * instance_cameraFromModel * vec4(modelPos, 1);
  color          = instance_color;
}
