#version 330 core

uniform sampler2D src;
uniform uvec2 frameBufferSize;
uniform float maxIntensity;

out vec4 fragment;

void main()
{
  vec4 src_color = texture(src, gl_FragCoord.xy / frameBufferSize);
  fragment = vec4(src_color.rgb / maxIntensity, src_color.a);
}
