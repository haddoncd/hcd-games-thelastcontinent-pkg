#version 330 core

uniform mat4 clipFromCamera;
uniform mat4 cameraFromModel;
uniform mat4 cameraFromModel_normals;
uniform vec3 uniformColor;

layout(location = 0) in vec4 modelPos;

smooth out vec3 color;
smooth out vec3 cameraPos;
flat   out vec3 cameraNormal;

void main()
{
  vec4 cameraPos4 = cameraFromModel * modelPos;
  gl_Position     = clipFromCamera * cameraPos4;
  color           = uniformColor;
  cameraPos       = cameraPos4.xyz;
  cameraNormal    = normalize(mat3(cameraFromModel_normals) * vec3(0, 0, 1));
}
