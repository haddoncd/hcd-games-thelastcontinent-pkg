#version 330 core

smooth in vec3 color;

out vec4 fragment;

void main()
{
  float alpha = 1.0;
  fragment = vec4(color, 1.0) * alpha;
}
