#version 330 core

uniform mat4 clipFromCamera;

layout(location = 0) in vec3 vertex_modelPos;
layout(location = 1) in vec3 vertex_modelNormal;
layout(location = 2) in vec3 vertex_color;
layout(location = 3) in mat4 instance_cameraFromModel;
layout(location = 7) in mat4 instance_cameraFromModel_normals;

smooth out vec3 color;
smooth out vec3 cameraPos;
flat   out vec3 cameraNormal;

void main()
{
  vec4 cameraPos4 = instance_cameraFromModel * vec4(vertex_modelPos, 1);
  gl_Position     = clipFromCamera * cameraPos4;
  color           = vertex_color;
  cameraPos       = cameraPos4.xyz;
  cameraNormal    = normalize(mat3(instance_cameraFromModel_normals) * vertex_modelNormal);
}
