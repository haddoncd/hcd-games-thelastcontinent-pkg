#version 330 core

layout(location = 0) in vec2 modelPos;

void main()
{
  gl_Position = vec4(modelPos, 0, 1.0);
}
