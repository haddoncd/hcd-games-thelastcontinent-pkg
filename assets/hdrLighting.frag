#version 330 core

layout (std140) uniform Lighting
{
  vec4  ambientColor;
  vec4  globalLightColor;
  vec4  globalLightCameraVector;
  float surfaceBlinnPhongMultiplier;
  float surfaceBlinnPhongExponent;
} lighting;

smooth in vec3 color;
smooth in vec3 cameraPos;
flat   in vec3 cameraNormal;

out vec4 fragment;

void main()
{
  vec3 view_direction = normalize(-cameraPos);
  vec3 view_light_halfway_direction = normalize(view_direction +
                                                lighting.globalLightCameraVector.xyz);
  float specular_intensity = pow(max(dot(view_light_halfway_direction, cameraNormal), 0.0),
                                 lighting.surfaceBlinnPhongExponent) *
                             lighting.surfaceBlinnPhongMultiplier;
  float diffuse_intensity = max(dot(lighting.globalLightCameraVector.xyz, cameraNormal), 0.0);
  vec3 lit_color = (specular_intensity * lighting.globalLightColor.rgb) +
                   ( ( (diffuse_intensity * lighting.globalLightColor.rgb) +
                       lighting.ambientColor.rgb ) *
                     color );
  float alpha = 1.0;
  fragment = vec4(lit_color, 1.0) * alpha;
}
