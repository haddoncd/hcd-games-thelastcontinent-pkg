#version 330 core

uniform sampler2D src;
uniform uvec2 frameBufferSize;

out vec4 fragment;

void main()
{
  fragment = texture(src, gl_FragCoord.xy / frameBufferSize);
}
