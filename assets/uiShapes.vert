#version 330 core

uniform mat4 clipFromCamera;

layout(location = 0) in vec2 vertex_modelPos;
layout(location = 1) in vec3 instance_color;
layout(location = 2) in mat4 instance_cameraFromModel;

smooth out vec3 color;

void main()
{
  vec4 cameraPos = instance_cameraFromModel * vec4(vertex_modelPos, 0, 1);
  gl_Position    = clipFromCamera * cameraPos;
  color          = instance_color;
}
